
import "react-native-gesture-handler";

import React,{useEffect, useState} from 'react';
import {AppState,Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import {useSelector} from 'react-redux'


import { NetworkConsumer } from 'react-native-offline'; 
import Offilne from "./pages/Offilne";
import OneSignal from 'react-native-onesignal';

import createRouter from './routes'
import AsyncStorage from '@react-native-community/async-storage';

import moment from 'moment';
import api from "./services/api";


function App({props}) {
     const [first,setFirst] = useState(null)
    const signed = useSelector(state => state.auth.signed)
   
    const user = useSelector(state => state.user)
    const auth = useSelector(state => state.auth.dados);

    const Routes = createRouter(signed,user,auth,first)

    const [status,setStatus] = useState(AppState.currentState)
   

    useEffect(()=>{
      async function setItems(){
        const jsonValue = JSON.stringify({
          datetime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
          os:  Platform.OS,
          device: DeviceInfo.getModel(),
        });
        await AsyncStorage.setItem('acessos', jsonValue);

        const primeiroAce =  await AsyncStorage.getItem('@primeiroAcesso')
        setFirst(primeiroAce)
        
      }
  
      setItems()
    },[signed])

    useEffect(()=>{
      if(signed){
        let externalUserId = `${user.id}`; 
        OneSignal.setExternalUserId(externalUserId); 
      }
    },[])

    
   AppState.addEventListener('change', _handleAppStateChange);

   
     

    async function _handleAppStateChange(nextAppState){
    
      if(nextAppState === 'background'){ 
        
        if(signed === true){
          const json = await AsyncStorage.getItem('acessos');
          const dados = JSON.parse(json)
      
          const dataStart = `${dados.datetime}`
          const dataEnd = `${moment(new Date()).format('YYYY-MM-DD HH:mm:ss')}` 
      
          await api.post(`v1/registerMeta?start_at=${dataStart}&end_at=${dataEnd}&os=${dados.os}&device=${dados.device}`) 
          await AsyncStorage.removeItem('acessos');
          
        }
        
      }else if(nextAppState === 'active'){
        const jsonValue = JSON.stringify({
          datetime: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
          os:  Platform.OS,
          device: DeviceInfo.getModel(),
        });
        await AsyncStorage.setItem('acessos', jsonValue);
        
     
      }
     
    }

  
    return (
      <NetworkConsumer>
      {({ isConnected }) => (
        isConnected ? (
          <Routes />
        ) : (
          <Offilne />
        )
      )}
    </NetworkConsumer>
  );
}

export default App
