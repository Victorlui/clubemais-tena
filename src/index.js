import 'react-native-gesture-handler';

import React, {useEffect} from 'react';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {SafeAreaView} from 'react-native';

import {NetworkProvider} from 'react-native-offline';

import codePush from 'react-native-code-push';
import OneSignal from 'react-native-onesignal';


import './config/ReactotronConfig';

import {store, persistor} from './store';
import App from './App';

import SplashScreen from 'react-native-splash-screen';

function Index() {
  useEffect(() => {
    SplashScreen.hide();
    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE,
    });

    OneSignal.init('7dc1ead7-1526-4549-846d-27e45bc81c0a');

 
    
  }, []);

  

  return (
    
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <NetworkProvider>
          <SafeAreaView style={{flex: 0, backgroundColor: '#3078be'}} />
          <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
            <App />
          </SafeAreaView>
        </NetworkProvider>
      </PersistGate>
    </Provider>
  );
}

export default Index;
