import axios from 'axios'

const api = axios.create({
    baseURL: 'https://tena.ciadetrade.eco.br/api/'
})

export default api;