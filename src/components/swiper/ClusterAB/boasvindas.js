import React from 'react';
import {
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    ImageBackground,
  } from 'react-native';

import ClusterAB from '../../../assets/clusterAB.png';

import styles from '../styles';

export default function BoaVindas({navigation}) {
  return (
    <View style={styles.child}>
    <View style={{width: 200}}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 38,
          color: '#004480',
          marginBottom: 10,
        }}>
        Olá Seja Bem Vindo!
      </Text>
    </View>

    <View style={{width: 300}}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          marginBottom: 10,
          color: '#6e6f71',
        }}>
        Para melhorar ainda mais sua experiência arraste para o lado e
        veja tudo que a plataforma poderá oferecer.
      </Text>
    </View>
    <ImageBackground
      source={ClusterAB}
      resizeMode="cover"
      style={{width: 300, height: 300}}>
      <></>
    </ImageBackground>

    <View style={{flexDirection:'row',marginTop:100,alignItems:'center',justifyContent:'space-between',width:'50%'}}>
      <View style={{backgroundColor:'gray',borderRadius:30,padding:5}}>
        <Text>1</Text>
      </View>
      <View>
        <Text>1</Text>
      </View>
      <View>
        <Text>1</Text>
      </View>
      <View>
        <Text>1</Text>
      </View>
    </View>
    
  </View>

  );
}
