import React from 'react';
import {
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    ImageBackground,
  } from 'react-native';

import Atividades from '../../../assets/atividades.png';

import styles from '../styles';
// import { Container } from './styles';

export default function ConteudosInterativos({navigation}) {
  return (
    <View style={styles.child}>
            <View style={{width: 200}}>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 38,
                  color: '#004480',
                  marginBottom: 10,
                }}>
                Conteúdos Interativos.
              </Text>
            </View>

            <View style={{width: 280}}>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 16,
                  marginBottom: 10,
                  color: '#6e6f71',
                }}>
                Os conteúdos dos níveis podem ser vídeo,images e jogos.
                Aproveitem o máximo dos recursos para evoluir.
              </Text>
            </View>
            <ImageBackground
              source={Atividades}
              resizeMode="cover"
              style={{width: 190, height: 300}}>
              <></>
            </ImageBackground>
          </View>
  );
}
