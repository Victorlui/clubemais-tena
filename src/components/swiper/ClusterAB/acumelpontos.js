import React from 'react';
import {
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    ImageBackground, 
  } from 'react-native'; 

import AcumulePontos from '../../../assets/acumule.png';

import styles from '../styles';

export default function ClusterAB() {
  return (
    <View style={styles.child}>
      <View style={{width: 200}}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 38,
            color: '#004480',
            marginBottom: 10,
          }}>
          Acumule pontos.
        </Text>
      </View>

      <View style={{width: 280}}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 16,
            marginBottom: 10,
            color: '#6e6f71',
          }}>
          Quando terminar de interagir com os conteúdos do seu nível os pontos
          serão computados no app.
        </Text>
      </View>
      <ImageBackground
        source={AcumulePontos}
        resizeMode="cover"
        style={{width: 190, height: 300}}>
        <></>
      </ImageBackground>
    </View>
  );
}
