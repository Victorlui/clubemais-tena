import React from 'react';
import { View } from 'react-native';

import styles from '../styles';

import MonitoreGer from '../../../assets/monitore.png'

export default function Welcome() {
  return (
    <View style={styles.child}>
    <View style={{width: 200}}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 38,
          color: '#004480',
          marginBottom: 10,
        }}>
        Olá Seja Bem Vindo!
      </Text>
    </View>

    <View style={{width: 280}}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          marginBottom: 10,
          color: '#6e6f71',
        }}>
        Para melhorar ainda mais sua experiência arraste para o lado e
        veja tudo que a plataforma poderá oferecer.
      </Text>
    </View>
    <ImageBackground
      source={MonitoreGer}
      resizeMode="cover"
      style={{width: 190, height: 300}}>
      <></>
    </ImageBackground>
  </View>
  );
}
