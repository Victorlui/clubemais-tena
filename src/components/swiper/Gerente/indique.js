import React from 'react';
import {View} from 'react-native';

import styles from '../styles';

import Indique from '../../../assets/indique.png'

export default function Indique() {
  return (
    <View style={styles.child}>
      <View style={{width: 240}}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 38,
            color: '#004480',
            marginBottom: 10,
          }}>
          Indique membros da sua equipe.
        </Text>
      </View>

      <View style={{width: 280}}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 16,

            color: '#6e6f71',
          }}>
          Sua equipe é responsavel pela evolução do ponto de venda, quanto mais
          pontuarem mais evoluida a sua loja será
        </Text>
      </View>
      <ImageBackground
                source={Indique}
                resizeMode="cover"
                style={{width: 190, height: 300}}>
                <></>
              </ImageBackground>
    </View>
  );
}
