import React from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';

import ResgatePontos from '../../assets/premios.png'

import styles from './styles'

export default function Resgate() {
  return (
    <View style={styles.child}>
    <View style={{width: 240}}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 38,
          color: '#004480',
          marginBottom: 10,
        }}>
        Resgate seus pontos!
      </Text>
    </View>

    <View style={{width: 280}}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 16,
          marginBottom: 10,
          color: '#6e6f71',
        }}>
        Após finalizar o nível e o pontos forem computados, você
        poderá resgata-los por prêmios
      </Text>
    </View>
    <ImageBackground
      source={ResgatePontos}
      resizeMode="cover"
      style={{width: 190, height: 300}}>
      <></>
    </ImageBackground>
  </View>
  );
}
