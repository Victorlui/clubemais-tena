import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:"row"
  },
  child: {
    height: height * 0.9,
    width:"100%",
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;