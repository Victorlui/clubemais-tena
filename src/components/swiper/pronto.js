import React from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';

import styles from './styles'

export default function Pronto() {
  return (
    <View style={styles.child}>
      <View style={{width: 240}}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 38,
            color: '#004480',
            marginBottom: 15,
          }}>
          Pronto.
        </Text>
      </View>
      <View style={{width: 240}}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 16,
            color: '#6e6f71',
            marginBottom: 15,
          }}>
          Você já está pronto para começar, clique abaixo e bons treinamentos
          para você!
        </Text>
      </View>

      <View style={{width: 280}}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 16,
            color: '#004480',
          }}>
          Caso precise ver essas telas novamente basta acessar o menu e clicar
          em como funciona!
        </Text>
      </View>

      <TouchableOpacity
        onPress={() => {
          setIndexPage(0);
          navigation.navigate('Level');
        }}>
        <Image source={Comecar} resizeMode="center" />
      </TouchableOpacity>
    </View>
  );
}
