import React, {Component} from 'react';
import {View, Text, ScrollView, Dimensions, StyleSheet} from 'react-native';

// API
import api from '../../services/api';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  );
};

export default class Terms extends Component {
  state = {
    accepted: false,
    regulamento: '',
  };

  async componentDidMount() {
    const response = await api.get('v1/regulamento');
    const data = response.data.regulamento.regulamento;
    this.setState({regulamento: data});
  }

  render() {
    return (
      <ScrollView
        style={styles.tcContainer}
        onScroll={({nativeEvent}) => {
          if (isCloseToBottom(nativeEvent)) {
            this.setState({
              accepted: false,
            });
          }
        }}>
        <View style={styles.container}>
          <Text style={styles.title}>Termos e condições</Text>

          <Text style={{color: '#222222'}}>{this.state.regulamento}</Text>
        </View>
      </ScrollView>
    );
  }
}

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 4,

    padding: 5,
    marginBottom: 10,
  },
  title: {
    fontSize: 22,
    alignSelf: 'center',
    color: '#3078be',
    marginVertical:10
  },
  tcP: {
    marginTop: 10,
    marginBottom: 10,
    fontSize: 12,
  },
  tcP: {
    marginTop: 10,
    fontSize: 12,
  },
  tcL: {
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 10,
    fontSize: 12,
  },
  tcContainer: {
    marginTop: 15,
    marginBottom: 15,
    height: height * 0.7,
    padding: 10,
  },

  button: {
    backgroundColor: '#136AC7',
    borderRadius: 5,
    padding: 10,
  },

  buttonDisabled: {
    backgroundColor: '#999',
    borderRadius: 5,
    padding: 10,
  },

  buttonLabel: {
    fontSize: 14,
    color: '#FFF',
    alignSelf: 'center',
  },
});
