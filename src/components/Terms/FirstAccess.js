import React, {Component, PropTypes} from 'react';
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage'

import Terms from './Terms';
import api from '../../services/api';
export default class FirstAccess extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      aceitarTermo: '',
    };
  }

  aceitar() {
    api.post('v1/regulamento');
  }

  componentDidMount() {
   AsyncStorage.getItem(this.props.pagekey, (err, result) => {
      if (err) {
      } else {
        if (result == null) {
          this.setModalVisible(true);
        }
      }
    });
    AsyncStorage.setItem(this.props.pagekey, JSON.stringify({value: 'true'}));
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <View style={{zIndex: 5, opacity: 1}}>
        <Modal
          animationType={'slide'}
          style={styles.ftreContainer}
          visible={this.state.modalVisible}>
          <View style={{flex: 5}}>
            <Terms />
          </View>

          <View style={{alignItems:'center',justifyContent:'center',marginBottom:10,backgroundColor:'transparent'}}>
            <TouchableOpacity
              onPress={() => {
                [
                  this.setModalVisible([
                    !this.state.modalVisible,
                  ]),
                  this.aceitar(),
                ];
              }}
             >
              <View style={styles.ftreExitButtonContainer}>
                <Text style={styles.ftreExitButtonText}>Aceitar</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ftreContainer: {
    backgroundColor: 'transparent',
    flex: 1,
    marginTop: 40,
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ftreExitButtonContainer: {
    width: 200,
    height: 40,
    backgroundColor: 'blue',
    borderRadius: 10,
    justifyContent: 'center',
    marginTop: 20,
  },
  ftreExitButtonText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
