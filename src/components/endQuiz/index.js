import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import Modal from 'react-native-modal';

import Lottie from 'lottie-react-native';
import done from '../../assets/done.json';
import Winner from '../../assets/trophy.json';

import Trofeu from '../../assets/trofeu.png';
import Tena from '../../assets/tena.png';

import Icon from 'react-native-vector-icons/MaterialIcons';

import api from '../../services/api';

export default function endQuiz({navigation}) {
  const items = navigation.getParam('items');
  const TempoDecorrido = navigation.getParam('TempoDecorrido');
  const Nota = navigation.getParam('results');
  const time = navigation.getParam('time');
  const [openModal, setOpenModal] = useState(false);
  const [modal, setModal] = useState(false);


  //finalizar atividade quado for imagem
  async function finalizaAtividadeQuiz() {
    await api.post(
      `/v1/resultGame?atividade_id=${items.id}&hits=${
        Nota.correctAnswers
      }&errors=${Nota.wrongAnswers}&total_time=${time}`,
    );

    if (items.status === 2) {
      navigation.navigate('Timeline');
    } else {
      try {
        await api.get('v1/pointsActivity');

        const response = await api.post(
          `v1/endAtividade?atividade_id=${items.id}&points=${items.points}`,
        );

        if (response.data.status === 'Nível Finalizado') {
          setModal(true);
        } else {
          setOpenModal(true);
        }
      } catch (error) {
        navigation.navigate('Timeline');
      }
    }
  }

  async function nivel() {
    const response = await api.get('v1/niveis');
    setModal(false);
    navigation.navigate('Level', {nivel: response.data.niveis});
  }

  function ModalFinalizarAtividade() {
    return (
      <Modal isVisible={modal} animationIn="fadeInLeft">
        <View style={styles.modal}>
          <Lottie
            source={Winner}
            autoPlay
            loop={false}
            resizeMode="contain"
            style={{height: 200}}
          />
          <Text style={{color: 'white', fontWeight: 'bold'}}>
            Parabéns você completou um nivel
          </Text>
          <TouchableOpacity
            onPress={() => {
              nivel();
            }}
            style={styles.close}>
            <Text style={styles.textButton}>OK</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  async function closeModalQuiz() {
    const ativiades = await api.get('v1/atividades');
    navigation.navigate('Timeline', {Change: ativiades.data.atividades});
    setOpenModal(false);
  }

  function ModalQuiz() {
    return (
      <Modal isVisible={openModal}>
        <View style={styles.modal}>
          <Lottie
            source={done}
            autoPlay
            loop={false}
            resizeMode="contain"
            style={{height: 300, width: 300}}
          />
          <Text style={{color: 'white', fontSize: 20}}>
            Atividade Concluída
          </Text>
          <TouchableOpacity
            style={{
              marginTop: 10,
              backgroundColor: '#3078be',
              width: '80%',
              padding: 5,
              borderRadius: 8,
              alignItems: 'center',
            }}
            onPress={() => {
              closeModalQuiz();
            }}>
            <Text style={{color: 'white', fontSize: 18}}>OK</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  return (
    <ScrollView>
      {ModalQuiz()}
      {ModalFinalizarAtividade()}
      <View style={styles.header}>
        <Text
          style={{
            textAlign: 'center',
            color: 'white',
            fontSize: 24,
            fontWeight: 'bold',
          }}>
          Vamos Jogar?
        </Text>
      </View>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={styles.imagem}>
          <Image source={Trofeu} resizeMode="contain" style={{height: 150}} />
        </View>
        <View style={styles.textWinner}>
          <Text
            style={{
              fontWeight: 'bold',
              color: '#242750',
              fontSize: 30,
              textAlign: 'center',
              marginBottom: 10,
            }}>
            Parabéns,você concluiu o jogo de perguntas !
          </Text>

          <View style={{marginTop: 5, alignItems: 'center'}}>
            <Text style={{fontWeight: 'bold'}}>Tempo decorrido:</Text>
            <Text style={{fontSize: 25}}>{TempoDecorrido}</Text>
          </View>

          <View style={{marginTop: 5, alignItems: 'center'}}>
            <Text style={{fontWeight: 'bold'}}>Acertos:</Text>
            <Text style={{fontSize: 25}}>{Nota.score}</Text>
          </View>

          {Nota.wrongAnswers > 0 && (
            <View style={{marginTop: 5, alignItems: 'center'}}>
              <Text style={{fontWeight: 'bold'}}>Erros:</Text>
              <Text style={{fontSize: 25}}>{Nota.wrongAnswers}</Text>
            </View>
          )}
        </View>
        <View style={styles.resultado}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              finalizaAtividadeQuiz();
            }}>
            <Icon name="keyboard-arrow-right" color="white" size={50} />
            <Text style={{color: '#8cbe4f', fontSize: 20, fontWeight: 'bold'}}>
              CONCLUIR
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.imagem}>
          <Image source={Tena} resizeMode="contain" style={{height: 60}} />
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  imagem: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  textWinner: {
    margin: 10,
    alignItems: 'center',
    fontSize: 16,
  },
  resultado: {
    backgroundColor: '#e6e6e6',
    marginBottom: 10,
    padding: 10,
    height: 180,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#242750',
    width: 200,
    height: 50,
    borderRadius: 8,
  },
  header: {
    backgroundColor: '#242750',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modal: {
    height: 320,
    width: '100%',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    fontSize: 18,
    color: '#FBCC00',
  },
  modal: {
    height: 320,
    width: '100%',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

endQuiz.navigationOptions = {
  header: null,
};
