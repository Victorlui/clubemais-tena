import React, {useState} from 'react';
import {View, StyleSheet, Text, Dimensions, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';

const {width, height} = Dimensions.get('window');

// LIB DE VIDEO
import VideoPlayer from 'react-native-video-controls';

import Modal from 'react-native-modal';

import Lottie from 'lottie-react-native';
import done from '../../assets/done.json';
import api from '../../services/api.js';


export default function ativityVideo({items, navigation}) {
  const [points, setPoints] = useState({});
  const [openModalFinalizaVideo, setOpenModalFinalizaVideo] = useState(false);
  const user = useSelector(state => state.user);


  async function closeModalVideo() {
    const ativiades = await api.get('v1/atividades');
    navigation.navigate('Timeline', {Change: ativiades.data.atividades});
    setOpenModalFinalizaVideo(false);
  }

  async function onVoltarVideo() {
    const response = await api.get('v1/atividades');
    navigation.navigate('Timeline', {atividades: response.data.atividades});
  }

  function ModalVideo() {
    return (
      <Modal isVisible={openModalFinalizaVideo}>
        <View style={styles.modal}>
          <Lottie
            source={done}
            autoPlay
            loop={false}
            resizeMode="contain"
            style={{height: 300, width: 300}}
          />
          <Text style={{color: 'white', fontSize: 20}}>
            Atividade Concluída
          </Text>
          <TouchableOpacity
            style={{
              marginTop: 10,
              backgroundColor: '#3078be',
              width: '80%',
              padding: 5,
              borderRadius: 8,
              alignItems: 'center',
            }}
            onPress={() => {
              closeModalVideo();
            }}>
            <Text style={{color: 'white', fontSize: 18}}>OK</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  //finalizar atividade quado for video
  async function finalizaVideo(id, points, status) {
    if (user.user.type === 1) {
      navigation.navigate('Timeline');
    }  else {
      try {
        const res = await api.get('v1/pointsActivity');
        setPoints(res.data);

        const response = await api.post(
          `v1/endAtividade?atividade_id=${id}&points=${points}`,
        );

        const ativiades = await api.get('v1/atividades');

        if (response.data.status === 'Nível Finalizado') {
          navigation.navigate('Timeline', {
            Change: ativiades.data.atividades,
            nivelFinalizado: 1,
          });
        } else {
          setOpenModalFinalizaVideo(true);
        }
      } catch (error) {
        navigation.navigate('Timeline');
      }
    }
  }

  return (
    <View style={{height: height}}>
        {ModalVideo()}
      <VideoPlayer
        source={{uri: `https://tena.ciadetrade.eco.br/${items.video}`}}
        onBack={() => {
          onVoltarVideo();
        }}
        disableVolume={true}
        disableSeekbar={true}
        disableFullscreen={true}
        fullScreen={false}
        toggleResizeModeOnFullscreen={false}
        resizeMode="contain"
        style={{
          width: width,
          height: height,
        }}
        customStyles={{
          width: '100%',
          height: '100%',
          flex: 1,
        }}
        // fullscreenOrientation={"landscape"}
        // onEnterFullscreen={}

        onEnd={() => finalizaVideo(items.id, items.points, items.status)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
    modal: {
        height: 320,
        width: '100%',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        
      },
})
