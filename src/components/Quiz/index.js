import React, {useState, useEffect} from 'react';

import {
  View,
  TouchableOpacity,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
} from 'react-native';

import Close from 'react-native-vector-icons/MaterialIcons';


import Tena from '../../assets/tena.png';
import Jogo from '../Jogo';


export default function Quiz({navigation}) {
  const items = navigation.getParam('items');

  return (
    <ScrollView>
      <View style={{flex: 1}}>
        {items.type === 1 && (
          <View style={{width: '100%', backgroundColor: '#3078be', height: 60}}>
            <View
              style={{
                width: '20%',
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Timeline');
                }}>
                <Close name="keyboard-arrow-left" color="white" size={40} />
              </TouchableOpacity>
            </View>
          </View>
        )}

        {items.type === 3 && (
          <View style={{width: '100%', backgroundColor: '#3078be', height: 60}}>
            <View
              style={{
                width: '20%',
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Timeline');
                }}>
                <Close name="keyboard-arrow-left" color="white" size={40} />
              </TouchableOpacity>
            </View>
          </View>
        )}

        <View style={{
                height:60,
                backgroundColor:"#242750"

              }}>
               <Text
              style={{
                textAlign: 'center',
                letterSpacing: 0.5,
                color: '#FFFFFF',
                fontWeight:"bold",
                fontSize: 25,
                padding:15
              }}>
              Vamos Jogar ? 
            </Text>
              </View>

        <View
          style={{
            padding: 10,
            borderWidth: 0,
            borderColor: '#ddd',
            margin: 10,
            borderRadius: 10,
          }}>
          <Text
            style={{
              textAlign: 'center',
              color: '#242750',
              fontSize: 22,
              lineHeight:28,
              fontWeight: '600',
              padding:10
            }}>
            {items.conteudo}
          </Text>
        </View>

        <View
          style={{
            alignItems: 'center',
            backgroundColor: '#e6e6e6',
          }}>
              <Jogo navigation={navigation} items={items} />
        </View>
        <View style={{alignItems: 'center', marginTop:20}}>
          <Image
            source={Tena}
            resizeMode="contain"
            style={{width: 150, height: 50}}
          />
        </View>
      </View>
    </ScrollView>
  );
}

Quiz.navigationOptions = {
  header: null,
};
