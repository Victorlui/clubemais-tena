import React from 'react';

import {useDispatch, useSelector} from 'react-redux';
import {signOut} from '../../store/modules/auth/actions';

import {View, StatusBar, TouchableOpacity, Text, Image} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Grad from 'react-native-vector-icons/Entypo';
import Coins from 'react-native-vector-icons/FontAwesome5';
import Logout from 'react-native-vector-icons/Ionicons';
import Tooltip from 'rn-tooltip';

import Tena from '../../assets/tena.png'

import {styles} from './styles';

export default function Header({navigation, profile, nome, points}) {
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();

  function logout() {
    dispatch(signOut());
  }

  return (
    <View style={styles.header}>
      <StatusBar backgroundColor="#3078be" barStyle="light-content" />
      <View style={styles.menu}>
        <View>
          <TouchableOpacity
            onPress={() => {
              navigation.toggleDrawer();
            }}>
            <Icon name="navicon" size={32} color="white" />
          </TouchableOpacity>
        </View>

        <View>
          <Image source={Tena} resizeMode='contain' style={{height:80,width:70}} />
        </View>
      

        <View style={styles.boxPoint}>
          {profile === true ? (
            <TouchableOpacity
              onPress={() => {
                logout();
              }}
              style={styles.profileIcon}>
              <Logout name="ios-log-out" color="#192a5b" size={22} />
            </TouchableOpacity>
          ) : (
            <View style={{alignItems:'center',justifyContent:'center',width:90}}>
              <Text style={styles.title}>{user.user.name.split(' ').slice(0, 1).join(' ')}</Text>
            </View>
          )}
          

          <View style={styles.options}>
            <Tooltip  backgroundColor={"#7bbe4b"} width={160} withOverlay={false} popover={<Text style={{color:"#FFFFFF"}}>Atividades realizadas!</Text>}>
                <Text style={{padding:0}} ><Grad name="graduation-cap" color="#3078be" size={22} /></Text>
            </Tooltip>
            {points && <Text style={{marginLeft: 4}}>{points.atividades}</Text>}
          </View>
          <View style={styles.options}>
            <Tooltip backgroundColor={"#7bbe4b"} width={250} withOverlay={false} popover={<Text style={{color:"#FFFFFF"}}>Estrelas acumuladas das atividades!</Text>}> 
            <Icon name="star" color="#ffe500" size={22} />
            </Tooltip>
            {points && <Text style={{marginLeft: 4}}>{points.points}</Text>}
          </View>
        </View>
      </View>
    </View>
  );
}
