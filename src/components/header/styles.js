import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  header: {
    backgroundColor: '#3078be',
    height: 70,
    padding: 10,
    justifyContent: 'center',
    shadowColor: '#000000',
  },

  menu: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  headerName: {
    width: '40%',
    alignItems: 'flex-end',
    padding: 2,
  },
  title: {
    color: '#222222',
    fontSize: 16,
    marginRight: 3,
  },
  boxPoint: {
    backgroundColor: 'white',
    padding: 10,
    flexDirection: 'row',
    borderRadius: 15,
    justifyContent: 'space-between',
  },
   options:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems:'center',
    padding: 5,
   },
   profileIcon:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    marginRight:20
   }
});
