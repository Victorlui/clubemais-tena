import React from 'react';

import { View, Image, StyleSheet, ActivityIndicator } from 'react-native';

// import { Container } from './styles';

function ProgressiveImage(props){

    return(
        <View style={styles.container}>
            <Image
            loadingIndicatorSource={<ActivityIndicator color="blue" />}
                {...props}
                source={props.thumbnailSource}
                style={props.style}
            />
        <Image
          {...props}
          source={props.source}
          style={[styles.imageOverlay, props.style]}
        />
        </View>
    )
}

const styles = StyleSheet.create({
    imageOverlay: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      top: 0,
    },
    container: {
      backgroundColor: '#e1e4e8',
    },
  });

export default ProgressiveImage;
