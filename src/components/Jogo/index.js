import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ActivityIndicator, Text, Button, Alert} from 'react-native';
import Question from '../Question';
import api from '../../services/api';


export default function Jogo({navigation, items}) {
  const [loading, setLoading] = useState(false);
  const [questions, setQuestions] = useState([]);
  const [current, setCurrent] = useState(0);
  const [HoraInicial, SetHoraInicial] = useState(0)
  const [correctScore, setCorrestScore] = useState(5);
  const [totalScore, setTotalScore] = useState(50);
  const [MultiAnswers, setMultiAnswers] = useState('');
  const [AlreadyAnswered, setAlreadyAnswered]= useState([]);
  const [results, setResults] = useState({
    score: 0,
    correctAnswers: 0,
    wrongAnswers: 0,
  });
  const [completed, setCompleted] = useState(false);
  const [error, setError] = useState('');
  const [Relatorio, setRelatorio]= useState([]);

  useEffect(() => {
    LoadPerguntas();
  }, []);

  async function LoadPerguntas() {
    setLoading(true);
    const response = await api.get(`v1/getQuiz?atividade_id=${items.id}`);

    

    setQuestions(response.data.data);

    setLoading(false);

    SetHoraInicial(+new Date())
  }

  function submitAnswer(index, answer,idQ) {
    const qtd = index + 1
    const question = questions[index];
    
    const CorrectAnswers = Object.values(question.correct_answer);

    const isCorrect = CorrectAnswers.includes(answer);

    const isWrong = !CorrectAnswers.includes(answer);

    const indexArray = Relatorio[index]

    if (isCorrect === false) {

      if (error != 'Resposta incorreta!' ){
        setError('Resposta incorreta!');
      }

      const pontosIncorreto = !isCorrect
        ? results.wrongAnswers + 1
        : results.wrongAnswers;

      setResults({score: results.score, correctAnswers: results.correctAnswers ,wrongAnswers: pontosIncorreto});

      if (question.incorrect_feeback){

        Alert.alert(
          'Resposta incorreta!',
          question.incorrect_feeback,
          [
            {text: 'Ok'},
          ]
        );
      }

      if (!indexArray){
        let answerResult = Relatorio

        let InternArray = {pergunta_id: idQ, resposta: 0}



        answerResult.push(InternArray)
        setRelatorio(answerResult)
      }

    } else {

      setError('');

      if (question.correct_feeback && !AlreadyAnswered.includes(answer)){
        Alert.alert(
          'Resposta correta!',
          question.correct_feeback,
          [
            {text: 'Ok', onPress: () => {
              if(AlreadyAnswered.length === CorrectAnswers.length){
                setCurrent(index + 1)
                setMultiAnswers('')
              }
            }},
          ]
        );
      }

      if (CorrectAnswers.length > 1){
        if (!AlreadyAnswered.includes(answer)){
          let answers = AlreadyAnswered
          answers.push(answer)
          setAlreadyAnswered(answers)
        } else {
          Alert.alert(
            'Resposta correta!',
            'Porém esta resposta já foi enviada, tente novamente!',
            [
              {text: 'Ok', onPress: () => {
                if(AlreadyAnswered.length === CorrectAnswers.length){
                  setCurrent(index + 1)
                  setMultiAnswers('')
                }
              }},
            ]
          );
        }
      }

      const pontos = isCorrect && error != 'Resposta incorreta!' ? results.score + 1 : results.score;

      const pontosCorreto = isCorrect
        ? results.correctAnswers + 1
        : results.correctAnswers;

      if (!question.correct_feeback){
        
        if(CorrectAnswers.length === 1){
          setCurrent(index + 1)
        }
        
        if(AlreadyAnswered.length === CorrectAnswers.length && CorrectAnswers.length > 1){
          setCurrent(index + 1)
          setMultiAnswers('')
        }
      } else {
        if(CorrectAnswers.length === 1){
          setCurrent(index + 1)
        }
      }


      if (CorrectAnswers.length > 1){
        setMultiAnswers('Você acertou ' + AlreadyAnswered.length + ' de ' + CorrectAnswers.length + ' respostas!')
      }

      setCompleted(qtd === questions.length ? true : false);
      setResults({score: pontos, correctAnswers: pontosCorreto, wrongAnswers: results.wrongAnswers});
      
      if (!indexArray){

        let answerResult = Relatorio

        let InternArray = {pergunta_id: idQ, resposta: 1}
        

        answerResult.push(InternArray)
       
      }
    }
  }

  async function sendRelatorio(Relatory,TimePer){
     await api.post(`v1/endQuiz`,{
       atividade_id:items.id,
       time_resolv:TimePer,
       relatorio:Relatory
     });
    
  }

  function endQuiz() {
    
    const HoraFinal = +new Date();

    let Tempo = HoraFinal - HoraInicial

    let min = Math.floor((Tempo/1000/60) << 0);
    let sec = Math.floor((Tempo/1000) % 60);

    if (Number(min) > 0) {
       var tempoDecorrido = min + ' minutos e ' + sec + ' segundos.';

    } else {

       var tempoDecorrido = sec + ' segundos.';
    }

    let answerResult = Relatorio

    let Time = 'time_resolv: ' + '00:' + min + ':' + sec
    let TimePer = '00:' + min + ':' + sec


    sendRelatorio(answerResult,TimePer)


    navigation.navigate('EndQuiz', {results: results, items: items, TempoDecorrido: tempoDecorrido,time:TimePer});
  }


  return (
    <View style={styles.container}>
      {!!loading && (
        <View style={styles.loadingQUiz}>
          <ActivityIndicator size="large" color="#3078be" />
          <Text style={{marginTop: 20, fontSize: 20, color: '#3078be'}}>
            Carregando Quiz
          </Text>
        </View>
      )}

      {!!questions.length > 0 && completed === false && (
        <Question
          onSelect={(answer,idQ) => {
            submitAnswer(current, answer,idQ);
          }}
          error={error}
          MultiOptions={MultiAnswers}
          question={questions[current]}
          correctPosition={Math.floor(Math.random() * 3)}
          current={current}
        />
      )}

      {completed === true && endQuiz()}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loadingQUiz: {
    flex: 1,
    alignItems: 'center',
    padding:10
  },
});
