import React from 'react';
import {useDispatch} from 'react-redux';
import {signOut} from '../../store/modules/auth/actions';

import {View, Text, Image, TouchableOpacity,Linking,ScrollView} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

import 'react-native-gesture-handler';

import Icon from 'react-native-vector-icons/FontAwesome';
import Fa from 'react-native-vector-icons/MaterialCommunityIcons';
import Call from 'react-native-vector-icons/MaterialIcons';

import logo from '../../assets/logo.png';
import api from '../../services/api';

import {styles} from './styles';

export default function menu({navigation, user}) {
  const dispatch = useDispatch();

  function goLevel() {
    navigation.navigate('Level');
  }

  async function getPointRewards() {
    try {
      const res = await api.get('v1/getSaldo');
      navigation.navigate('Rewards', {saldo: res.data.saldo});
    } catch (error) {
      navigation.navigate('Rewards', {saldo: 0});
    }
  }

  async function getPointExtracts() {
    const response = await api.get('v1/extrato');
    try {
      const res = await api.get('v1/getSaldo');
      navigation.navigate('Extracts', {
        extract: response.data,
        saldo: res.data.saldo,
      });
    } catch (error) {
      navigation.navigate('Extracts', {extract: response.data, saldo: 0});
    }
  }

  async function goRecomendetion() {
    try {
      const response = await api.get('v1/getSaldo');
      navigation.navigate('Recommendation', {saldo: response.data.saldo});
    } catch (error) {
      navigation.navigate('Recommendation', {saldo: 0});
    }
  }

  function goHelp() {
    navigation.navigate('FirstLogin2');
  }

  function goCall() {
    navigation.navigate('Call');
  }

  function goProfile() {
    navigation.navigate('Profile');
  }

  async function goMeusResgates() {
    const response = await api.get('v1/rescues');
    navigation.navigate('MyRescues', {resgates: response.data.resgates});
  }

  async function goLogout() {
    await AsyncStorage.removeItem('@primeiroAcesso');
    dispatch(signOut());
  }

  return (
  <ScrollView showsVerticalScrollIndicator={false} style={{flex:1,backgroundColor: '#3078be'}}>
      <View style={styles.container}>
      <View>
        <Image source={logo} style={styles.logo} resizeMode="contain" />
        <Text style={styles.title}>VANTAGENS E BENEFÍCIOS EXCLUSIVOS</Text>
      </View>

      <View style={styles.nav}>
        <View>
          <TouchableOpacity
            style={styles.links}
            onPress={() => {
              goLevel();
            }}>
            <View style={styles.icon}>
              <Icon name="level-up" color="white" size={32} />
            </View>
            <View style={{width: '80%', marginLeft: 20}}>
              <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
                Níveis
              </Text>
            </View>
          </TouchableOpacity>

         
          {user.user.store.cluster.name === 'B' ? null : (
            <TouchableOpacity
              style={styles.links}
              onPress={() => {
                getPointRewards();
              }}>
              <View style={{display: 'flex', alignItems: 'center'}}>
                <Icon name="trophy" color="white" size={32} />
              </View>

              <View style={{width: '80%', marginLeft: 20}}>
                <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
                 Resgatar Créditos
                </Text>
              </View>
            </TouchableOpacity>
          )}

          {user.user.store.cluster.name === 'B' ? null : (
            <TouchableOpacity
              style={styles.links}
              onPress={() => {
                goMeusResgates();
              }}>
              <View style={{display: 'flex', alignItems: 'center'}}>
                <Call name="card-giftcard" color="white" size={32} />
              </View>

              <View style={{width: '80%', marginLeft: 20}}>
                <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
                  Meus Resgates
                </Text>
              </View>
            </TouchableOpacity>
          )}

          {user.user.store.cluster.name === 'B' ? null : (
            <TouchableOpacity
              style={styles.links}
              onPress={() => {
                getPointExtracts();
              }}>
              <View style={styles.icon}>
                <Icon name="file-text" color="white" size={32} />
              </View>
              <View style={{width: '80%', marginLeft: 20}}>
                <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
                  Extrato
                </Text>
              </View>
            </TouchableOpacity>
          )}

          {user.user.type === 2  ? (
            null
          ) :  
          <TouchableOpacity
          style={styles.links}
          onPress={() => {
            goRecomendetion();
          }}>
          <View style={styles.icon}>
            <Fa name="hand-pointing-up" color="white" size={32} />
          </View>
          <View style={{width: '80%', marginLeft: 20}}>
            <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
              Indicações
            </Text>
          </View>
        </TouchableOpacity>}

           <TouchableOpacity
          style={styles.links}
          onPress={() => {
            goHelp();
          }}>
          <View style={styles.icon}>
            <Icon name="cog" color="white" size={32} />
          </View>
          <View style={{width: '80%', marginLeft: 20}}>
            <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
              Como Funciona
            </Text>
          </View>
        </TouchableOpacity>

        

          <TouchableOpacity
            style={styles.links}
            onPress={() => {
              goCall();
            }}>
            <View style={styles.icon}>
              <Call name="local-phone" color="white" size={32} />
            </View>
            <View style={{width: '80%', marginLeft: 20}}>
              <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
                Fale Conosco
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.links}
            onPress={() => {
              navigation.navigate('Faq')
            }}>
            <View style={styles.icon}>
              <Fa name="help-circle" color="white" size={32} />
            </View>
            <View style={{width: '80%', marginLeft: 20}}>
              <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
                Conteúdo complementar
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.links}
            onPress={() => {
              Linking.openURL('http://tena.ciadetrade.eco.br/catalogos/2020-digital.pdf');
            }}>
            <View style={styles.icon}>
              <Icon name="book" color="white" size={32} />
            </View>
            <View style={{width: '80%', marginLeft: 20}}>
              <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
              Catálogo de produtos
              </Text>
            </View>
          </TouchableOpacity>



          <TouchableOpacity
            style={styles.links}
            onPress={() => {
              goProfile();
            }}>
            <View style={styles.icon}>
              <Call name="account-circle" color="white" size={32} />
            </View>
            <View style={{width: '80%', marginLeft: 20}}>
              <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
                Meus Dados
              </Text>
            </View>
          </TouchableOpacity>

          

          <TouchableOpacity
            style={styles.links}
            onPress={() => {
              goLogout();
            }}>
            <View style={styles.icon}>
              <Fa name="logout" color="white" size={32} />
            </View>
            <View style={{width: '80%', marginLeft: 20}}>
              <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
                Sair
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  </ScrollView>
  );
}
