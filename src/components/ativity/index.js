import React from 'react';

import {View, TouchableOpacity, ScrollView, SafeAreaView} from 'react-native';

import Close from 'react-native-vector-icons/MaterialIcons';
import api from '../../services/api';

//Components
import AtivityImg from '../ativityImg';
import AtivityQuiz from '../ativityQuiz';
import AtivityVideo from '../ativityVideo';
import AtivityMemoryGame from '../ativityMemoryGame';

let i = 0;

export default function Ativity({navigation}) {
  const items = navigation.getParam('items');

  async function goTimeline() {
    const response = await api.get('v1/atividades');
    navigation.navigate('Timeline', {atividades: response.data.atividades});
  }

  return (
    <SafeAreaView>
      <ScrollView>

        <View style={{flex: 1}}>
          {items.type === 1 && (
            <>
              <View
                style={{width: '100%', backgroundColor: '#3078be', height: 60}}>
                <View
                  style={{
                    width: '20%',
                    height: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      goTimeline();
                    }}>
                    <Close name="keyboard-arrow-left" color="white" size={40} />
                  </TouchableOpacity>
                </View>
              </View>

              <AtivityImg items={items} navigation={navigation} />
            </>
          )}

          {items.type === 2 && (
            <AtivityVideo items={items} navigation={navigation} />
          )}

          {items.type === 3 && (
            <>
              <View
                style={{width: '100%', backgroundColor: '#3078be', height: 60}}>
                <View
                  style={{
                    width: '20%',
                    height: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      goTimeline();
                    }}>
                    <Close name="keyboard-arrow-left" color="white" size={40} />
                  </TouchableOpacity>
                </View>
              </View>

              <AtivityQuiz items={items} navigation={navigation} />
            </>
          )}

          {items.type === 4 && (
              <AtivityMemoryGame items={items} navigation={navigation} i={i++} />

          )}  
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

Ativity.navigationOptions = {
  header: null,
};
