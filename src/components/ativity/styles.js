import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#40739e',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
    elevation:5
  },
  boxPoints: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
  textPoints: {
    color: '#fff',
    fontSize: 20,
    marginLeft: 5,
    marginRight: 5,
  },
  points: {
    color: '#fff',
    fontSize: 20,
    marginRight: 5,
  },
  modal: {
    height: 320,
    width: '100%',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    
  },
});

export default styles;
