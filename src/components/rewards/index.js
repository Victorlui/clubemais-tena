import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  Image,
  Animated,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import {TextInputMask} from 'react-native-masked-text';

import sale from '../../assets/sale.png';
import api from '../../services/api';
import Modal from 'react-native-modal';

import {WSnackBar, WToast} from 'react-native-smart-tip';

export default function Rewards({itens,onPress}) {
  const [reward, setReward] = useState([]);
  const [visible, setVisible] = useState(false);
  const [items, setItems] = useState();
  const [resgate, setResgate] = useState([]);
  const [cell, setCell] = useState('');
  const [error,setError] = useState()

  function closeModal() {
    setVisible(false);
  }

  function openModal(itens) {
    setVisible(true);
    setItems(itens);
    setError()
  }

  async function onSubmit(items) {
    if(cell === '' ){
      const toastOpts = {
        data:"Preencha um número de telefone",
        textColor: '#fff',
        backgroundColor: 'red',
        duration: WToast.duration.LONG, //1.SHORT 2.LONG
        position: WToast.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
      };
      WSnackBar.show(toastOpts);
      closeModal()
    }else{
      try {
        const cell1 = cell.replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, '');
        const response = await api.post(`v1/rescue?reward_id=${items.id}&phone=${cell1}`);
  
        setResgate([...resgate, response.data.resgate]);
        closeModal()
        const toastOpts = {
          data:`Você resgatou ${response.data.resgate.value} reais.`,
          textColor: '#fff',
          backgroundColor: 'green',
          duration: WToast.duration.LONG, //1.SHORT 2.LONG
          position: WToast.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        };
        WSnackBar.show(toastOpts);
        onPress();
      } catch (error) {
        closeModal()
        const toastOpts = {
          data: error.response.data.message,
          textColor: '#fff',
          backgroundColor: 'red',
          duration: WToast.duration.LONG, //1.SHORT 2.LONG
          position: WToast.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        };
        WSnackBar.show(toastOpts);
        onPress();
        setResgate([]);
      }
    }
  }

  function ModalRescue(){
    return(
      <Modal isVisible={visible}>
      {items && (
        <View style={styles.modal}>
          <View style={styles.close}>
            <TouchableOpacity
              onPress={() => {
                closeModal();
              }}>
              <Icon name="close" size={20} color="red" />
            </TouchableOpacity>
          </View>

          <View style={{alignItems: 'center'}}>
           
            <Image
              style={{width: 150, height: 100}}
              resizeMode="contain"
              source={{uri: `https://tena.ciadetrade.eco.br/${items.image}`}}
            />
          </View>

          <View style={{alignItems: 'center'}}>
            <View style={styles.real}>
              <Text>R$</Text>
              <Text style={styles.paid}>{items.value}</Text>
            </View>
          </View>

          <TextInputMask
            type={'cel-phone'}
            options={{
              maskType: 'BRL',
              withDDD: true,
              dddMask: '(99) ',
            }}
            style={{
              marginTop: 10,
              backgroundColor: '#fff',
              borderRadius: 10,
              borderWidth: 0.75,
              borderColor: '#d3d3d3',
              textAlign:"center",
              fontSize:18,
              height:50
            }}
            value={cell}
            onChangeText={text => {
              setCell(text);
            }}
            placeholder="Digite um número de celular"
            returnKeyType="next"
          />

          <TouchableOpacity
            onPress={() => onSubmit(items)}
            style={styles.button}>
            <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18}}>
              Resgatar
            </Text>
          </TouchableOpacity>

          {error && (
            <View style={{marginTop:20,alignItems:'center'}}>
              <Text style={{color:'red',fontSize:18}}>{error}</Text>
            </View>
          )}
        </View>
      )}
    </Modal>
    )
  }

  return(
    itens.rewards.map(item => (
      <TouchableOpacity onPress={() => openModal(item)}>
        {ModalRescue()}
          <View key={itens.id} style={styles.card}>
            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
              <View>
                <Text>{item.details}</Text>
                <View style={styles.price}>
                  <View style={styles.real}>
                    <Text>R$</Text>
                    <Text style={styles.paid}>{item.value}</Text>
                  </View>
              </View>
            </View>
            <View>
              <Image
                  source={{uri: `https://tena.ciadetrade.eco.br/${item.image}`}}
                  style={{width: 80, height: 60, resizeMode: 'contain'}}
                  
                />
            </View>
           
            </View>
  
          
          </View>
      </TouchableOpacity>
    ))
  )
 
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#f2f2f2',
    padding: 10,
    borderWidth: 1,
    borderColor: '#d3d3d3',
    marginTop: 10,
    borderRadius: 20,
    margin: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.37,
    shadowRadius: 5.49,
  },
  price: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  paid: {
    fontSize: 32,
    color: '#222222',
  },
  real: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 8,
    height: 320,
    padding: 10,
  },
  close: {
    alignItems: 'flex-end',
  },
  button: {
    backgroundColor: '#1247c2',
    marginTop: 10,
    alignItems: 'center',
    padding: 5,
    borderWidth: 1,
    borderColor: '#fff',
    shadowColor: '#000',
    borderRadius: 20,
    elevation: 5,
  },
});
