import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import { styles } from './styles';

import Modal from 'react-native-modal'

import Lottie from 'lottie-react-native';
import done from '../../assets/done.json';

import api from '../../services/api';


export default function Game({navigation,id,points}) {
  const [pergunta1, setPergunta1] = useState(true);
  const [pergunta2, setPergunta2] = useState(false);
  const [pergunta3, setPergunta3] = useState(false);
  const [pergunta4, setPergunta4] = useState(false);
  const [pergunta5, setPergunta5] = useState(false);
  const [pergunta6, setPergunta6] = useState(false);
  const [pergunta7, setPergunta7] = useState(false);
  const [pergunta8, setPergunta8] = useState(false);
  const [pergunta9, setPergunta9] = useState(false);
  const [pergunta10, setPergunta10] = useState(false);
  const [final, setFinal] = useState(false)
  const [openModal, setOpenModal] = useState(false);
  const [openModalQuiz, setOpenModalQuiz] = useState(false)
 

  function resposta1(resposta) {
    if (resposta === 'Estar') {
      setPergunta1(false)
      setPergunta2(true) 
    } else {
     setOpenModal(true)
    }
  }

  function resposta2(resposta) {
    if (resposta === 'Estar') {
        setPergunta2(false)
        setPergunta3(true)
    } else {
        setOpenModal(true)
    }
  }

  function resposta3(resposta) {
    if (resposta === 'Estar') {
        setPergunta3(false)
        setPergunta4(true)
    } else {
        setOpenModal(true)
    }
  }

  function resposta4(resposta) {
    if (resposta === 'Estar') {
        setPergunta4(false)
        setPergunta5(true)
    } else {
        setOpenModal(true)
    }
  }

  function resposta5(resposta) {
    if (resposta === 'Estar') {
        setPergunta5(false)
        setPergunta6(true)
    } else {
        setOpenModal(true)
    }
  }

  function resposta6(resposta) {
    if (resposta === 'Estar') {
        setPergunta6(false)
        setPergunta7(true)
    } else {
        setOpenModal(true)
    }
  }

  function resposta7(resposta) {
    if (resposta === 'Estar') {
        setPergunta7(false)
        setPergunta8(true)
    } else {
        setOpenModal(true)
    }
  }

  function resposta8(resposta) {
    if (resposta === 'Estar') {
        setPergunta8(false)
        setPergunta9(true)
    } else {
        setOpenModal(true)
    }
  }

  function resposta9(resposta) {
    if (resposta === 'Estar') {
        setPergunta9(false)
        setPergunta10(true)
    } else {
        setOpenModal(true)
    }
  }

  function resposta10(resposta) {
    if (resposta === 'Ser') {
        setPergunta10(false)
        setFinal(true)
    } else {
        setOpenModal(true)
    }
  }

  function finalizarQuiz(){
    setPergunta1(true)
        setFinal(false)
  }
  function pergunta() {
    return (
      pergunta1 === true && (
        <View style={styles.questions}>
          <View style={styles.questionsTitle}> 
           <Text style={styles.textQuestion}>Obstrução do canal da uretra</Text>
          </View>
          <View style={styles.buttons}>
            <View style={styles.buttonAwnser}>
              <TouchableOpacity
              hitSlop={{top:20,bottom:20,left:100,right:100}}
                onPress={() => {
                  resposta1('Ser');
                }}>
                <Text styles={styles.respsText}>SER</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.buttonAwnser2}>
              <TouchableOpacity
              hitSlop={{top:20,bottom:20,left:100,right:100}}
                onPress={() => {
                  resposta1('Estar');
                }}>
                <Text styles={styles.respsText}>ESTAR</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )
    );
  }

  function perguntaSegunda() {
    return (
      pergunta2 === true && (
        <View style={styles.questions}>
        <View style={styles.questionsTitle}> 
          <Text style={styles.textQuestion}>Gravidez ou parto</Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.buttonAwnser}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta2('Ser');
              }}>
              <Text styles={styles.respsText}>SER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonAwnser2}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta2('Estar');
              }}>
              <Text styles={styles.respsText}>ESTAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      )
    );
  }

  function perguntaTerceira() {
    return (
      pergunta3 === true && (
       
        <View style={styles.questions}>
        <View style={styles.questionsTitle}> 
         <Text style={styles.textQuestion}> Obesidade</Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.buttonAwnser}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta3('Ser');
              }}>
              <Text styles={styles.respsText}>SER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonAwnser2}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta3('Estar');
              }}>
              <Text styles={styles.respsText}>ESTAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      )
    );
  }

  function perguntaQuarta() {
    return (
        
      pergunta4 === true && (
        <View style={styles.questions}>
        <View style={styles.questionsTitle}> 
         <Text style={styles.textQuestion}>Enfraquecimento dos músculos pélvicos </Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.buttonAwnser}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta4('Ser');
              }}>
              <Text styles={styles.respsText}>SER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonAwnser2}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta4('Estar');
              }}>
              <Text styles={styles.respsText}>ESTAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      )
    );
  }

  function perguntaQuinta() {
    return (
      pergunta5 === true && (
      
        <View style={styles.questions}>
        <View style={styles.questionsTitle}> 
         <Text style={styles.textQuestion}>  Irritação da bexiga </Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.buttonAwnser}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta5('Ser');
              }}>
              <Text styles={styles.respsText}>SER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonAwnser2}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                  
                resposta5('Estar');
              }}>
              <Text styles={styles.respsText}>ESTAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      )
    );
  }

  function perguntaSexta() {
    return (
      pergunta6 === true && (
        
        <View style={styles.questions}>
        <View style={styles.questionsTitle}> 
         <Text style={styles.textQuestion}>Infecção urinária </Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.buttonAwnser}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta6('Ser');
              }}>
              <Text styles={styles.respsText}>SER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonAwnser2}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta6('Estar');
              }}>
              <Text styles={styles.respsText}>ESTAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      )
    );
  }

  function perguntaSetima() {
    return (
      pergunta7 === true && (
        
        <View style={styles.questions}>
        <View style={styles.questionsTitle}> 
         <Text style={styles.textQuestion}>Constipação </Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.buttonAwnser}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta7('Ser');
              }}>
              <Text styles={styles.respsText}>SER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonAwnser2}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta7('Estar');
              }}>
              <Text styles={styles.respsText}>ESTAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      )
    );
  }

  function perguntaOitava() {
    return (
      pergunta8 === true && (
        
        <View style={styles.questions}>
        <View style={styles.questionsTitle}> 
         <Text style={styles.textQuestion}>Medo, estresse e ansiedade </Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.buttonAwnser}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta8('Ser');
              }}>
              <Text styles={styles.respsText}>SER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonAwnser2}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta8('Estar');
              }}>
              <Text styles={styles.respsText}>ESTAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      )
    );
  }


  function perguntaNona() {
    return (
      pergunta9 === true && (
        
        <View style={styles.questions}>
        <View style={styles.questionsTitle}> 
         <Text style={styles.textQuestion}>Suplementos vitamínicos </Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.buttonAwnser}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta9('Ser');
              }}>
              <Text styles={styles.respsText}>SER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonAwnser2}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta9('Estar');
              }}>
              <Text styles={styles.respsText}>ESTAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      )
    );
  }

  function perguntaDecima() {
    return (
      pergunta10 === true && (
        
        <View style={styles.questions}>
        <View style={styles.questionsTitle}> 
         <Text style={styles.textQuestion}>Medicamentos para doenças cardíacas </Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.buttonAwnser}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta10('Ser');
              }}>
              <Text styles={styles.respsText}>SER</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.buttonAwnser2}>
            <TouchableOpacity
            hitSlop={{top:20,bottom:20,left:100,right:100}}
              onPress={() => {
                resposta10('Estar');
              }}>
              <Text styles={styles.respsText}>ESTAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      )
    );
  }

  function finalAtividade() {
    return (
      final === true && (
        <View style={{alignItems:'center'}}>
            <View style={{width:'80%',alignItems:'center', marginTop:80, }}>
                <Text style={{textAlign:'center', fontSize:25, textAlign:"center", color:'#FFFFFF'}} >Parabéns, você acertou todos os itens e agora sabe mais sobre incontinência.</Text>
            </View>

            
              <TouchableOpacity hitSlop={{top:20,bottom:20,left:100,right:100}} style={styles.buttonFinalizar} onPress={() => {endAtividade()}}>
                <Text style={{color:'white',fontSize:16}}>Finalizar atividade</Text>
              </TouchableOpacity>
            

            
        </View>
      )
    );
  }

  async function endAtividade(){
    try {
        await api.get(
          'https://tena.ciadetrade.eco.br/api/v1/pointsActivity',
        );
       
        const response = await api.post(
          `https://tena.ciadetrade.eco.br/api/v1/endAtividade?atividade_id=${id}&points=${points}`,
        );
  
        const ativiades = await api.get('v1/atividades');
        
  
        if (response.data.status === 'Nível Finalizado') {
          navigation.navigate('Timeline', {
            Change: ativiades.data.atividades,
            nivelFinalizado: 1,
          });
        } else {
          setOpenModalQuiz(true);
        }
      } catch (error) {
        navigation.navigate('Timeline');
      }
    
  }

  async function closeModalImagem() {
    const ativiades = await api.get('v1/atividades');
    navigation.navigate('Timeline', {Change: ativiades.data.atividades});
    setOpenModalQuiz(false);
  }
  return (
    <View>
      {pergunta()}
      {perguntaSegunda()}
      {perguntaTerceira()}
      {perguntaQuarta()}
      {perguntaQuinta()}
      {perguntaSexta()}
      {perguntaSetima()}
      {perguntaOitava()}
      {perguntaNona()}
      {perguntaDecima()}
      {finalAtividade()}

      <Modal isVisible={openModal}>
          <Text></Text>
          <View style={{backgroundColor:'white',borderRadius:8,padding:10}}>
              
              <View style={{alignItems:'center'}}>
                  <Text style={{color:'red',fontSize:18}}>Resposta Errada</Text>
              </View>
              <View style={{width:'100%',alignItems:'center',marginTop:10,borderRadius:8,padding:10}}>
                <TouchableOpacity hitSlop={{top:20,bottom:20,left:100,right:100}} onPress={()=>{setOpenModal(false)}}>
                    <Text style={{color:'#3078be'}}>OK</Text>
                </TouchableOpacity>
              </View>
          </View>
      </Modal>

      <Modal isVisible={openModalQuiz}>
        <View style={styles.modal}>
          <Lottie
            source={done}
            autoPlay
            loop={false}
            resizeMode="contain"
            style={{height: 300, width: 300}}
          />
          <Text style={{color: 'white', fontSize: 20}}>
            Atividade Concluída
          </Text>
          <TouchableOpacity
            style={{
              marginTop: 10,
              backgroundColor: '#3078be',
              width: '80%',
              padding: 5,
              borderRadius: 8,
              alignItems: 'center',
            }}
            onPress={() => {
              closeModalImagem();
            }}>
            <Text style={{color: 'white', fontSize: 18}}>OK</Text>
          </TouchableOpacity>
        </View>
      </Modal>

    </View>
  );
}
