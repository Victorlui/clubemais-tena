import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  questions: {  
    alignItems: 'center',
    justifyContent: 'center',
    height: 150,
    marginTop: 35,

  },
  respsText:{
    color:'#FF0000'
  },
  questionsTitle: {
    alignSelf:"flex-start",
    backgroundColor: '#FFFFFF',
    padding: 15,
    width: '90%',
    borderTopRightRadius:10,
    borderBottomRightRadius:10,
    alignItems: 'center',
    marginBottom: 10,
    borderWidth:1,
    borderColor:'#d3d3d3',
   
  },
  textQuestion:{
    fontSize:18,
    color:'#1E3A89',
  },
  buttons: {
    flex:1,
    alignItems:"center",
    flexDirection: 'row',
    alignItems: 'center',
    marginTop:20

    },
  buttonAwnser:{
     margin:10,
     borderWidth:1,
     borderColor:'#d3d3d3',
     padding:10,
     borderRadius: 8,
     width:100,
     alignItems:'center',
     backgroundColor:'#73bd4b',
  },
  buttonAwnser2:{
    margin:10,
    borderWidth:1,
    borderColor:'#d3d3d3',
    padding:10,
    borderRadius: 8,
    width:100,
    alignItems:'center',
    color:'#FFFFFF',
    backgroundColor:'#1e3a89',
 },
  buttonFinalizar:{
      backgroundColor:'#73bd4b',
      alignItems:'center',
      padding:10,
      width:'40%',
      borderRadius: 8,
      marginTop:30
  },
  modal: {
    height: 320,
    width: '100%',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    
  },
});
