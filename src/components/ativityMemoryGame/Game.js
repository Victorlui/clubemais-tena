import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Platform,
  ImageBackground,
  TouchableHighlight,
  Dimensions,
  Image,
  ScrollView
} from 'react-native';

import {useSelector} from 'react-redux';

import Tena from '../../assets/logoteste.png';
import Lottie from 'lottie-react-native';
import done from '../../assets/done.json';
import Modal from 'react-native-modal';
import Winner from '../../assets/trophy.json';

import Icon from 'react-native-vector-icons/MaterialIcons';

import helper from './helper';

import BotaoQuiz from '../../assets/botaoQuiz.png';

//import {level1} from './api';
import Card from './Card';
import api from '../../services/api';

let tamanho = '';
let int = 1;

const height = Dimensions.get('window').height;

const Game = ({level1, items, navigation}) => {
  const [openModalImagem, setOpenModalImagem] = useState(false);
  const [modalWinner,setModalWinner] = useState(false)
  const [points, setPoints] = useState({});
  const user = useSelector(state => state.user);

  const [currentSelection, setCurrentSelection] = useState([]);
  const [selectedPairs, setSelectedPairs] = useState([]);
  const [scores, setScores] = useState(0);
  const [gameOver, setGameOver] = useState('');

  const [loading, setLoading] = useState(false);

  const [cards, setCards] = useState([]);

  const [level, setLevel] = useState('');

  useEffect(() => {
    loadCard();
    setLevel(1);
  }, []);

  function loadCard() {
    setLoading(true);

    tamanho = level1[0].pontos;
    let clone = JSON.parse(JSON.stringify(level1[0].data));

    clone.map(obj => {
      let id = Math.random()
        .toString(36)
        .substring(7);
      obj.id = id;
      obj.is_open = false;
    });

    clone = clone.shuffle();

    setCards(clone);
    setScores(0);
    setCurrentSelection([]);
    setSelectedPairs([]);
    setLoading(false);
    int++;
  }

  //click do cartao
  function clickCard(id) {
    let selected_pairs = selectedPairs;
    let current_selection = currentSelection;
    let score = scores;

    

    let index = cards.findIndex(card => {
      return card.id === id;
    });

    let newArr = [...cards];

    if (
      newArr[index].is_open === false &&
      selected_pairs.indexOf(newArr[index].name) === -1
    ) {
      // copying the old datas array
      newArr[index].is_open = true;
      //setCards(newArr)

      current_selection.push({
        index: index,
        name: newArr[index].name,
      });

      if (currentSelection.length === 2) {
        if (current_selection[0].name === current_selection[1].name) {
          score += 1;
          selected_pairs.push(newArr[index].name);
        } else {
          newArr[current_selection[0].index].is_open = false;

          // copying the old datas array
          setTimeout(() => {
            newArr[index].is_open = false;
            setCards(newArr);
          }, 500);
        }
        current_selection = [];
      }

      setScores(score);
      setCards(newArr);
      setCurrentSelection(current_selection);
    }
  }

  //resetar
  function resetCards() {
    let novo = cards.map(obj => {
      obj.is_open = false;
      return obj;
    });

    setCards(novo);
    setScores(0);
    setCurrentSelection([]);
    setSelectedPairs([]);
  }

  //renderiza as o cartao
  function renderRows() {
    let contents = getRowContents(cards);
    return contents.map((cards, index) => {
      return (
        <View key={index} style={styles.row}>
          {renderCards(cards)}
        </View>
      );
    });
  }

  //rendereiz os cartaos
  function renderCards(cards) {
    return cards.map((card, index) => {
      return (
        <Card
          key={index}
          src={card.src}
          name={card.name}
          level={level}
          text={card.text}
          is_open={card.is_open}
          type={card.type}
          clickCard={() => {
            clickCard(card.id);
          }}
        />
      );
    });
  }

  //renderiza os components
  function getRowContents(cards) {
    let contents_r = [];
    let contents = [];
    let count = 0;
    cards.forEach(item => {
      count += 1;
      contents.push(item);
      if (count === cards.length) {
        contents_r.push(contents);
        count = 0;
        contents = [];
      }
    });

    return contents_r;
  }

 async  function nextLevel() {
    await api.post(
      `/v1/resultGame?atividade_id=${
        items.id
      }&hits=${scores}&errors=0&total_time=0`,
    );
    setLoading(true);
    const item = level1.filter(item => {
      return item.level === int;
    });

    if (item[0] === undefined) {
      setGameOver('Você venceu!!');
    } else if (item[0].level === int) {
      setLevel(item[0].level);
      tamanho = item[0].pontos;
      let clone = JSON.parse(JSON.stringify(item[0].data));

      clone.map(obj => {
        let id = Math.random()
          .toString(36)
          .substring(7);
        obj.id = id;
        obj.is_open = false;
      });

      clone = clone.shuffle();
      setCards(clone);
      setScores(0);
      setCurrentSelection([]);
      setSelectedPairs([]);
      setLoading(false);
    }

    int++;
  }

  //finalizar atividade quado for game
  async function finalizar() {
    

    if (user.user.type === 1) {
      navigation.navigate('Timeline');
    } else if (items.status === 2) {
      navigation.navigate('Timeline');
    } else {
      try {

        const response = await api.post(
          `v1/endAtividade?atividade_id=${items.id}&points=${items.points}`,
        );

        await api.get('v1/atividades');

        if (response.data.status === 'Nível Finalizado') {
          setModalWinner(true)
        } else {
          setOpenModalImagem(true);
        }
      } catch (error) {
        navigation.navigate('Timeline');
      }
    }
  }

  function ModalImagem() {
    return (
      <Modal isVisible={openModalImagem}>
        <View style={styles.modal}>
          <Lottie
            source={done}
            autoPlay
            loop={false}
            resizeMode="contain"
            style={{height: 300, width: 300}}
          />
          <Text style={{color: 'white', fontSize: 20}}>
            Atividade Concluída
          </Text>
          <TouchableOpacity
            style={styles.buttonClose}
            onPress={() => {
              closeModalImagem();
            }}>
            <Text style={{color: 'white', fontSize: 18}}>OK</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  function Msg() {
    return (
      <Modal isVisible={true}>
        <View style={{backgroundColor: '#FFF', padding: 10, borderRadius: 10}}>
          <Text style={{fontSize: 18, fontWeight: 'bold', margin: 5}}>
            Parabéns
          </Text>
          <Text style={{fontSize: 16, margin: 5}}>
            Você passou para o próximo nivel.
          </Text>
          <TouchableOpacity
            style={{
              margin: 10,
              borderWidth: 1,
              borderColor: '#d3d3d3',
              padding: 10,
              alignItems: 'center',
              borderRadius: 10,
            }}
            onPress={() => {
              setLevel(level => level + 1);
              nextLevel();
            }}>
            <Text>OK</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  async function nivel() {
    const response = await api.get('v1/niveis');
    setModalWinner(false);
    navigation.navigate('Level', {nivel: response.data.niveis});
  }

  function modalWiner(){
    return(
      <Modal isVisible={modalWinner} animationIn="fadeInLeft">
      <View style={styles.modal}>
        <Lottie
          source={Winner}
          autoPlay
          loop={false}
          resizeMode="contain"
          style={{height: 200}}
        />
        <Text style={{color: 'white', fontWeight: 'bold'}}>
          Parabéns você completou um nivel
        </Text>
        <TouchableOpacity
          onPress={() => {
            nivel();
          }}
          style={styles.close}>
          <Text style={styles.textButton}>OK</Text>
        </TouchableOpacity>
      </View>
    </Modal>
    )
  }


  return (
   
    <ImageBackground
      source={require('../../assets/back.png')}
      style={styles.container}>
      {ModalImagem()}
     {modalWiner()}
      <View style={styles.header}>
        {gameOver !== '' ? null : scores === tamanho && Msg()}
      </View>

      <View style={styles.body}>
        {gameOver !== '' ? (
          <>
            <Text style={{fontSize: 32, color: '#FFF', fontWeight: 'bold'}}>
              {gameOver}
            </Text>
            {items.status === 2 ? (
              <TouchableOpacity
                onPress={() => {
                  int = 1;
                  navigation.navigate('Timeline');
                }}
                style={{
                  backgroundColor: '#FFF',
                  padding: 10,
                  borderRadius: 5,
                  margin: 10,
                }}>
                <Text
                  style={{
                    color: '#24447f',
                    fontSize: 18,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  Atividade concluida, clique para voltar
                </Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  int = 1;
                  finalizar();
                }}
                style={{
                  backgroundColor: '#FFF',
                  padding: 10,
                  borderRadius: 5,
                  margin: 10,
                }}>
                <Text
                  style={{
                    color: '#24447f',
                    fontSize: 18,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  Finalizar atividade
                </Text>
              </TouchableOpacity>
            )}
          </>
        ) : (
          renderRows()
        )}
      </View>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        {gameOver === '' ? (
          <>
            <Image
              source={{}}
              resizeMode="contain"
              style={{width: 110, height: 30}}
            />

            <TouchableOpacity
              style={{padding: 5}}
              onPress={() => {
                resetCards();
              }}>
              <Image
                source={require('../../assets/backbutton.png')}
                resizeMode="contain"
                style={{width: 100, height: 50}}
              />
            </TouchableOpacity>
          </>
        ) : null}
      </View>
     
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#50b3d7',
    height: height,
  },
  modal: {
    height: 320,
    width: '100%',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    flex: 1,
    justifyContent: 'space-between',
    resizeMode: 'cover',
    alignItems: 'center',
  },
  header: {
    paddingTop: Platform.OS === 'ios' ? 40 : 20,
    paddingRight: 20,
    width: '100%',
    alignItems: 'flex-end',
  },
  buttonNextLevel: {
    borderWidth: 1,
    borderColor: '#FFF',
    borderRadius: 5,
    padding: 10,
    backgroundColor: '#FFF',
  },
  textNextLevel: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#24447f',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  body: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '95%',
  },
});

export default Game;
