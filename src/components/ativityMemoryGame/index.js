import React,{useEffect,useState} from 'react';
import { View,Text,TouchableOpacity,Image,StyleSheet,Dimensions,ImageBackground } from 'react-native';
import api from '../../services/api';

import Game from './Game';

import BotaoQuiz from '../../assets/botaoQuiz.png';
import Tena from '../../assets/tena.png';
const height = Dimensions.get('window').height 

const AtivityMemoryGame = ({navigation,i,items}) => {

    const [level1,setLevel1] = useState([])
    const [select,setSelect] = useState(false)
    const [loading,setLoading] = useState(false)


   useEffect(()=>{
    setLoading(true)
       async function LoadGame(){
         
        const res = await api.get(`v1/getMemory?atividade_id=${items.id}`)
        setLevel1(res.data.game)
        setLoading(false)

       }

       LoadGame()
   },[i])
  
   

  return (
    
      level1.length === 0 ? (
        null
      ):(
       select ? (
        <Game level1={level1} items={items} navigation={navigation} />
       ):(
        <ImageBackground source={require('../../assets/back.png')} style={styles.container}>
                <Text style={{fontSize:32,color:'#FFF'}}>Iniciar jogo</Text>
                  <TouchableOpacity
                    style={{marginTop: 20}}
                    onPress={() => {
                     setSelect(true)
                    }}>
                    <Image
                      source={BotaoQuiz}
                      resizeMode="contain"
                      style={{width: 150, height: 150}}
                    />
                  </TouchableOpacity>
               
         </ImageBackground>
       )
      )
    
  );
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'space-between',
      resizeMode: 'cover',
      alignItems: 'center',
      justifyContent:"center",
      height:height
  },
})

export default AtivityMemoryGame;