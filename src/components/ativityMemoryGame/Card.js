import React from 'react';
import {
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  View,
  ImageBackground,
  Platform
} from 'react-native';

let width = 0;
let height = 0;

const Card = ({is_open, src, name, text, clickCard, level}) => {
  width = level === 1 ? 120 : level === 2 ? Platform.OS === 'ios' ? 90 : 100 : level === 3 ? 100 : 0;
  height = level === 1 ? 150 : level === 2 ? 140 : level === 3 ? 150 : 0;

  return is_open ? (
    <View style={{margin: 5}}>
      <Image
        source={{uri: src}}
        resizeMode="contain"
        style={{width: width, height: height}}
      />
    </View>
  ) : (
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={() => {
        clickCard();
      }}>
      <ImageBackground
        source={require('../../assets/card1.png')}
        resizeMode="contain"
        style={{
          width: width,
          height: height,
          alignItems: 'center',
          justifyContent: 'center',
          
        }}>
        <Text style={{fontSize: 60, color: '#FFF', fontWeight: 'bold'}}>?</Text>
      </ImageBackground>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3e7a22',
    margin: 5,
    borderRadius: 10,
    borderWidth: 6,
    borderColor: '#71bf44',
  },
  card_text: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFF',
  },
});

export default Card;
