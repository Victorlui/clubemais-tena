import React, {useState} from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity, Dimensions,Platform, ScrollView} from 'react-native';
import {useSelector} from 'react-redux';

const {width, height} = Dimensions.get('window');

import Modal from 'react-native-modal';

import api from '../../services/api';

import Lottie from 'lottie-react-native';
import done from '../../assets/done.json';
import Winner from '../../assets/trophy.json';


import AutoHeightImage from 'react-native-auto-height-image';

let ratio = 0;

export default function ativityImg({items, navigation}) {
  const [openModalImagem, setOpenModalImagem] = useState(false);
  const [modalWinner,setModalWinner] = useState(false)
  const user = useSelector(state => state.user);

  const [bloqueio,setBloqueio] = useState(false)

  const [teste, setTeste] = useState(0);
  const [w,setW] = useState(0)
  const [h,setH] = useState(0)


  Image.getSize(`https://tena.ciadetrade.eco.br/${items.image_post}`, (srcWidth, srcHeight) => {

    const maxHeight = Dimensions.get('window').height; // or something else
    const maxWidth = Dimensions.get('window').width;
  
  setW(srcWidth)
  setH(srcHeight)
   

    ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

    setTeste (parseFloat(ratio.toFixed(2)))

   
    }, (error) => {
      console.error(`Couldn't get the image size: ${error.message}`);
    });

  async function closeModalImagem() {
    const ativiades = await api.get('v1/atividades');
    navigation.navigate('Timeline', {Change: ativiades.data.atividades});
    setOpenModalImagem(false);
  }

  function ModalImagem() {
    return (
      <Modal isVisible={openModalImagem}>
        <View style={styles.modal}>
          <Lottie
            source={done}
            autoPlay
            loop={false}
            resizeMode="contain"
            style={{height: 300, width: 300}}
          />
          <Text style={{color: 'white', fontSize: 20}}>
            Atividade Concluída
          </Text>
          <TouchableOpacity
            style={styles.buttonClose}
            onPress={() => {
              closeModalImagem();
            }}>
            <Text style={{color: 'white', fontSize: 18}}>OK</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  //finalizar atividade quado for imagem
  async function finalizaImagem(id, points, status) {
     setBloqueio(true)
    if (user.user.type === 1) {
      navigation.navigate('Timeline');
    } else if (status === 2) {
      navigation.navigate('Timeline');
    } else {
      try {

        const response = await api.post(
          `v1/endAtividade?atividade_id=${id}&points=${points}`,
        );

         await api.get('v1/atividades');
         
        if (response.data.status === 'Nível Finalizado') {
            setModalWinner(true)
        } else {
          setOpenModalImagem(true);
        }
      } catch (error) {
        navigation.navigate('Timeline');
      }
    }
  }

  async function nivel() {
    const response = await api.get('v1/niveis');
    setModalWinner(false);
    navigation.navigate('Level', {nivel: response.data.niveis});
  }

  function modalWiner(){
    return(
      <Modal isVisible={modalWinner} animationIn="fadeInLeft">
      <View style={styles.modal}>
        <Lottie
          source={Winner}
          autoPlay
          loop={false}
          resizeMode="contain"
          style={{height: 200}}
        />
        <Text style={{color: 'white', fontWeight: 'bold'}}>
          Parabéns você completou um nivel
        </Text>
        <TouchableOpacity
          onPress={() => {
            nivel();
          }}
          style={styles.close}>
          <Text style={styles.textButton}>OK</Text>
        </TouchableOpacity>
      </View>
    </Modal>
    )
  }

  return (
    <View style={{alignItems:'center'}}>
      {ModalImagem()}
      {modalWiner()}
      <View style={styles.content}>
        <Text style={styles.title}>{items.name}</Text>
        <Text style={styles.contentText}>{items.Conteúdo}</Text>
      </View>
      <View style={styles.body}>
        <Text style={styles.textBody}>
          Clique na imagem para completar sua atividade
        </Text>
      </View>
    
       <View style={{flex:1,width:'98%'}}>
       {/* <TouchableOpacity
          disabled={bloqueio}
          onPress={() => finalizaImagem(items.id, items.points, items.status)}>
            
          <Image
            style={{width:'100%' ,height:Platform.OS === 'ios' ? h*0.50 : h*1}}
            resizeMode={Platform.OS === 'ios' ? "center" :"contain"}
            source={{uri: `https://tena.ciadetrade.eco.br/${items.image_post}`}}
          />
          
        </TouchableOpacity> */}

     
        <TouchableOpacity
          disabled={bloqueio}
          onPress={() => finalizaImagem(items.id, items.points, items.status)}>

          <AutoHeightImage
            width={width}
            source={{uri: `https://tena.ciadetrade.eco.br/${items.image_post}`}}
            
          />

        </TouchableOpacity>

       </View>

      
    
      

   
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    paddingTop: 5,
    borderWidth: 0,
    borderColor: '#ddd',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
  },
  contentText: {
    textAlign: 'center',
    letterSpacing: 0.5,
    fontSize: 18,
    

  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 24,
    color: '#3078be',
  },
  body: {
    alignItems: 'center',
    marginVertical: 0,
   
  },
  textBody: {
    fontSize: 16,
    textAlign: 'center',
    color: '#3078be',
    letterSpacing: 0.5,
  },
  img: {
    width: width,    
   
    
  },
  img2: {
    height: 270,
    width: "100%",
    marginBottom: 10,
    tintColor:'gray',
  },
  modal: {
    height: 320,
    width: '100%',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonClose:{
    marginTop: 10,
    backgroundColor: '#3078be',
    width: '80%',
    padding: 5,
    borderRadius: 8,
    alignItems: 'center',
  },
  close: {

    borderTopColor: '#d3d3d3',
    alignItems: 'center',
    width: '100%',
    marginTop:10
  },
  textButton:{
    fontSize:18,
    color:"#FBCC00"
  },
});
