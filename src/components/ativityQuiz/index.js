import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';

import BotaoQuiz from '../../assets/botaoQuiz.png';
import Tena from '../../assets/tena.png';


export default function ativityQuiz({items,navigation}) {
  return (
    <View>
             <View
              style={{
                flex: 1,
                backgroundColor: '#FFFFFF',
                textAlign: 'center',
              }}>
              <View
                style={{
                  height: 60,
                  backgroundColor: '#242750',
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    letterSpacing: 0.5,
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                    fontSize: 25,
                    padding: 15,
                  }}>
                  Vamos Jogar ?
                </Text>
              </View>
              <Text
                style={{
                  textAlign: 'center',
                  letterSpacing: 0.5,
                  color: '#242750',
                  fontWeight: '800',
                  fontSize: 25,
                  marginTop: 10,
                  padding: 24,
                }}>
                {items.Conteúdo}
              </Text>
            </View>
              <View
                style={{
                  alignItems: 'center',
                  marginVertical: 20,
                  padding: 10,
                  backgroundColor: '#e6e6e6',
                }}>
                <View>
                  <Text
                    style={{
                      textAlign: 'center',
                      color: '#242750',
                      fontSize: 19,
                      fontWeight: 'bold',
                      padding: 15,
                    }}>
                    As condições irão aparecer na tela, Toque nas opções para
                    escolher sua resposta!
                  </Text>
                </View>
                <View style={{alignItems: 'center'}}>
                  <TouchableOpacity
                    style={{marginTop: 20}}
                    onPress={() => {
                      navigation.navigate('Quiz', {items: items});
                    }}>
                    <Image
                      source={BotaoQuiz}
                      resizeMode="contain"
                      style={{width: 150, height: 150}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{alignItems: 'center', marginTop: 5}}>
                <Image
                  source={Tena}
                  resizeMode="contain"
                  style={{width: 150, height: 50}}
                />
              </View>
            
    </View>
  );
}
