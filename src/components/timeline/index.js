import React, {useState, useEffect, useMemo} from 'react';
import {useSelector} from 'react-redux';
import api from '../../services/api';
import {
  View,
  Text,
  StatusBar,
  Image,
  TouchableOpacity,
  Animated,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import styles from './styles';

import AvatarImage from '../../assets/avatar1.png';
import AvatarQuiz from '../../assets/avatar2.png';
import AvatarVideo from '../../assets/avatar3.png';

import {WSnackBar, WToast} from 'react-native-smart-tip';

import {Grayscale} from 'react-native-color-matrix-image-filters';

import ProgressiveImage from '../Image/index';

let curtis = [];


export default function Posts({navigation, items, id, clickLike}) {
  const [offset, setOffset] = useState(new Animated.ValueXY({x: 0, y: 50}));
  const [opacity, setOpacity] = useState(new Animated.Value(0));
  const [like, setLike] = useState([]);
  const [curti, setCurti] = useState([]);
  const [view, setView] = useState([]);
  const [atividade, setAtividade] = useState();
  const [estado, setEstado] = useState(false);
  const [likeAtividade, setLikeAtividade] = useState([]);
  const user = useSelector(state => state.user);

  const {width, height} = Dimensions.get('window');


  curtis = items.likes.filter(l => l.user_id === user.user.id);


  useEffect(() => {
    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 20,
      }),

      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
      }),
    ]).start();
  }, []);

  if (items.type == 1) {
    var avatar = AvatarImage;
  } else if (items.type == 2) {
    var avatar = AvatarVideo;
  } else {
    var avatar = AvatarQuiz;
  }

  //começar uma atividade
  async function startAtividade(id, nivel_id, atividades, itens) {
    if (user.user.type === 1) {
      navigation.navigate('Ativity', {items: itens});
    } else if (itens.status === 2) {
      navigation.navigate('Ativity', {items: itens});
    } else if (itens.status === 1) {
      navigation.navigate('Ativity', {items: itens});
    } else {
      try {
        await api.post(`v1/view?atividade_id=${itens.id}`);
        await api.post(
          `v1/startAtividade?atividade_id=${itens.id}&nivel_id=${nivel_id}`,
        );
        navigation.navigate('Ativity', {items: itens});
      } catch (error) {
        if (error.response.data.status === 'Inicie a primeira atividade') {
          const snackBarOpts = {
            data: error.response.data.status,
            position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
            duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
            textColor: '#fff',
            backgroundColor: 'red',
          };

          WToast.show(snackBarOpts);
        } else if (
          error.response.data.status ===
          'Este módulo ainda não está liberado, todos os usuários desta loja devem estar no mesmo nível que você.'
        ) {
          const snackBarOpts = {
            data: error.response.data.status,
            position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
            duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
            textColor: '#fff',
            backgroundColor: 'red',
          };
          WToast.show(snackBarOpts);
        }
      }
    }
  }

  return (
    id === items.nivel_id && (
      <Animated.View
        style={[{transform: [{translateY: offset.y}]}, {opacity: opacity}]}
        key={items.id}>
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" backgroundColor="white" />
          <View style={styles.header}>
            <View>
              {items.status === null ? (
                <Grayscale>
                  <Image
                    source={avatar}
                    style={{width: 48, resizeMode: 'contain'}}
                  />
                </Grayscale>
              ) : (
                <Image
                  source={avatar}
                  style={{width: 48, resizeMode: 'contain'}}
                />
              )}
            </View>
            <View style={styles.title}>
              <Text style={{fontWeight: 'bold'}}>{items.name}</Text>
              <Text style={styles.description}>{items.title}</Text>
            </View>
          </View>

          <View>
            {items.status === null ? (
              <Grayscale>
                <TouchableOpacity
                  onPress={() =>
                    startAtividade(
                      items.id,
                      items.nivel_id,
                      items.atividades_users,
                      items,
                    )
                  }>
                  <Image
                    style={{height: 410, margin: 0}}
                    resizeMode="contain"
                    source={{
                      uri: `https://tena.ciadetrade.eco.br/${items.image}`,
                    }}
                  />
                </TouchableOpacity>
              </Grayscale>
            ) : (
              <TouchableOpacity
                onPress={() =>
                  startAtividade(
                    items.id,
                    items.nivel_id,
                    items.atividades_users,
                    items,
                  )
                }>
                <ProgressiveImage
                  style={{height: 410, margin: 0}}
                  resizeMode="contain"
                  thumbnailSource={{
                    uri: `https://tena.ciadetrade.eco.br/${items.image}`,
                  }}
                  source={{
                    uri: `https://tena.ciadetrade.eco.br/${items.image}`,
                  }}
                />
              </TouchableOpacity>
            )}
          </View>

          {items.status === 2 && (
            <View
              style={{
                backgroundColor: '#7bbe4b',
                marginTop: 0,
                marginBottom: 10,
                alignItems: 'center',
                padding: 7,
              }}>
              <Text style={{color: 'white', fontSize: 15}}>
                Atividade Concluida
              </Text>
            </View>
          )}

          <View style={styles.footer}>
            <View style={styles.boxLike}>
              {curtis.length === 0 ? (
                 <Icon
                 style={styles.icon}
                 name="like2"
                 size={26}
                 color="black"
                 onPress={() => {
                   clickLike(items.id);
                 }}
               />
              ):(
                <Icon
                style={styles.icon}
                name="like2"
                size={26}
                color="blue"
                onPress={() => {
                  clickLike(items.id);
                }}
              />
              )}
              <Text style={{fontWeight: 'bold'}}>{items.likes.length}</Text>

              <Text style={{marginLeft: 5}}>Curtidas</Text>

              {items.views.length === 0 ? (
                <Text style={{fontWeight: 'bold', marginLeft: 10}}>0</Text>
              ) : (
                <Text style={{fontWeight: 'bold', marginLeft: 10}}>
                  {items.views.length}
                </Text>
              )}

              <Text style={{marginLeft: 5}}>Visualização</Text>
            </View>

            {user.user.type === 1 ? null : (
              <>
                {items.status === null && (
                  <View style={styles.points}>
                    <Icon name="star" color="#ffe500" size={20} />
                    <Text
                      style={{color: '#fff', marginLeft: 5, marginRight: 5}}>
                      {items.points}
                    </Text>
                    <Text style={{color: '#fff', marginRight: 5}}>
                      Estrelas
                    </Text>
                  </View>
                )}

                {items.status === 1 && (
                  <View style={styles.points}>
                    <Icon name="star" color="#ffe500" size={20} />
                    <Text
                      style={{color: '#fff', marginLeft: 5, marginRight: 5}}>
                      {items.points}
                    </Text>
                    <Text style={{color: '#fff', marginRight: 5}}>
                      Estrelas
                    </Text>
                  </View>
                )}
              </>
            )}
          </View>
        </View>
      </Animated.View>
    )
  );
}

Posts.navigationOptions = {
  header: null,
};
