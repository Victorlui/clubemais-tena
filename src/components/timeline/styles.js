import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    marginTop: 0,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: 5,
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,

  },
  title: {
    width:"85%",
    padding:5
    
  },
  time: {
    fontSize: 10,
  },
  description: {
    fontSize: 13,
    marginTop:3,
    flexWrap:'wrap'
  },
  icon: {
    marginRight: 15,
    marginLeft:10,
  },
  points: {
    backgroundColor: '#20315f',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    padding: 10,
    marginTop:5,
  },

  boxLike: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modal: {
    backgroundColor: 'white',
    height: 100,
    width: '100%',
    borderRadius: 8,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
  },
  warning: {
    textAlign: 'center',
    marginTop: 15,
    fontSize: 16,
  },
  close: {
    borderTopWidth: 1,
    borderTopColor: '#d3d3d3',
    alignItems: 'center',
    width: '100%',
  },
  textButton:{
    fontSize:16,
    color:"red"
  }
});

export default styles;
