import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image, Dimensions,ActivityIndicator} from 'react-native';


export default function Question({
  current,
  question,
  onSelect,
  correctPosition,
  error,
  MultiOptions
}) {

  const [answer, setAnswer] = useState(null);
  const [loading, setLoading] = useState(true);

  var ImageSource = 'https://tena.ciadetrade.eco.br/' + question.image
  
  let styleError = 'none';

  if (error) {
    styleError = 'flex';
  }

  let styleMessage = 'none';

  if (MultiOptions) {
    styleMessage = 'flex';
  }
  
  function renderOptions(question) {
    
    const result = [];


    question.answers.forEach((item, index) => {
     
      
      let key = `${question.id}-${index}`;

      let styleButton = '';

      if (index === 0) {
        styleButton = '#242750';
      } else if (index === 1) {
        styleButton = '#49b8ef';
      } else if (index === 2) {
        styleButton = '#242750';
      } else if (index === 3) {
        styleButton = '#49b8ef';
      }

      result.push(
        <TouchableOpacity
          style={{
            backgroundColor: styleButton,
            borderRadius: 8,
            padding: 10,
            alignItems: 'center',
            justifyContent: 'center',
            margin: 5,
            width: '100%',
          }}
          onPress={() => {
            onSelect(item,question.id);
          }}
          key={key}>
          <Text style={styles.radioText}>{item}</Text>
        </TouchableOpacity>,
      );
    });

    return result;
  }

  function renderImg(){
    return(
      question.image && (
        <Image style={{alignSelf: 'center', height: 350, width: 350, resizeMode: "contain",marginTop:10}} source={{uri: ImageSource}}/>
        
      )
    )
  }

  return (
    <View style={{ padding: 18}}>

        {question.type && question.type === 1 && (
          <Text style={{textAlign: 'center', color: '#959595', fontSize: 18, marginBottom: 5}}>
            Condição:
          </Text>
        )}

        
          <View
            style={{
              backgroundColor: 'white',
              padding: 10,
              borderWidth: 1,
              borderColor: '#d3d3d3',
              borderRadius: 8,
          }}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: '#465497',
                textAlign: 'center',
              }}>
              {question.question}
            </Text>
          </View>
        
              {renderImg()}

      <View style={{margin: 10}}>
        <Text
          style={{textAlign: 'center', color: '#242750', fontWeight: 'bold'}}>
          Clique nos botões abaixo:
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          width:'100%'
        
        }}>
        {renderOptions(question)}
      </View>

      <View>
        <Text
          style={{
            flex: 1,
            flexDirection: 'row',
            textAlign: 'center',
            color: 'white',
            fontSize: 15,
            marginTop: 20,
            height: 40,
            padding: 10,
            backgroundColor: 'green',
            display: styleMessage
          }}>
          {MultiOptions}
        </Text>
      </View>

      <View>
        <Text
          style={{
            flex: 1,
            flexDirection: 'row',
            textAlign: 'center',
            color: 'white',
            fontSize: 15,
            marginTop: 20,
            height: 40,
            padding: 10,
            backgroundColor: '#FF0000',
            display: styleError,
          }}>
          {error}
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  groupButton: {
    marginTop: 20,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  buttons: {
    width: '45%',
    padding: 10,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonQuestion: {
    backgroundColor: '#242750',
    borderRadius: 8,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
    width: '50%',
  },
  buttonNext: {
    backgroundColor: '#3078be',
    borderRadius: 8,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
  },
  radioText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 22,
  },
});
