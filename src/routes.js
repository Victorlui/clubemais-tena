import 'react-native-gesture-handler';

import React from 'react';

//Rotas
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';

//Pages
import Login from './pages/Login';
import FirstAcess from './pages/FirstAcess';
import Timeline from './pages/Timeline';
import CreatePassword from './pages/CreatePassword';
import Rewards from './pages/Rewards';
import Posts from './components/timeline';
import Level from './pages/Level';
import Extracts from './pages/Extracts';
import Recommendation from './pages/Recommendation';
import Ativity from './components/ativity';
import Profile from './pages/Profile';
import Call from './pages/Call';
import FirstLogin from './pages/FirstLogin';
import FirstLogin2 from './pages/FirstLogin2';
import Quiz from './components/Quiz';
import EndQuiz from './components/endQuiz';
import MyRescues from './pages/MyRescues';
import Faq from './pages/Faq';
import ResetPassword from './pages/ResetPassword'

//Menu
import Menu from './components/menu';


export default (isSignIn = false, user, auth,first) =>


  createAppContainer(
    createStackNavigator(
      {
        SignIn: createStackNavigator(
          {
            Login, 
            FirstAcess,
            ResetPassword,
            CreatePassword,
           
          },
          {
            navigationOptions: {
              header: null,
            },
          },
        ),

        Ativity: createStackNavigator(
          {
            Ativity,
          },
          {
            navigationOptions: {
              header: null,
            },
          },
        ),

        Posts: createStackNavigator(
          {
            Posts,
          },
          {
            navigationOptions: {
              header: null,
            },
          },
        ),

        Quiz: createStackNavigator(
          {
            Quiz,
          },
          {
            navigationOptions: {
              header: null,
            },
          },
        ),

        EndQuiz: createStackNavigator(
          {
            EndQuiz,
          },
          { 
            navigationOptions: {
              header: null,
            },
          },
        ),

        FirstLogin: createStackNavigator(
          {
          
            FirstLogin,
          },
          { 
            navigationOptions: {
              header: null,
            },
          },
        ),


        Menu: createDrawerNavigator(
          {
            
            Level,
            FirstLogin2,
            Call,
            Rewards,
            Timeline,
            Extracts,
            Recommendation,
            Profile,
            MyRescues,
            Faq
          },
          {
            navigationOptions: {
              header: null,
            },
            drawerWidth: 250,
            drawerType: 'slide',
            contentComponent: ({navigation}) => (
              <Menu navigation={navigation} user={user} />
            ),
          },
        ),
      },
      {
        navigationOptions: {
          header: null,
        },
        initialRouteName: isSignIn ? first !== null ? 'Menu' : 'FirstLogin' : 'SignIn',
      },
    ),
  );
