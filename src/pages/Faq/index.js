import React,{useState,useEffect} from 'react';
import { View, Text, ScrollView } from 'react-native';


import api from '../../services/api';

import styles from './styles';
import Header from '../../components/header';


export default function Faq({navigation}) {

    const [faqs, setFaqs] = useState([]);
    const [points, setPoints] = useState();

    useEffect(()=>{
        async function loadFaqs(){
            const res = await api.get('v1/faqs');
            const po = await api.get('/v1/pointsActivity');
            setPoints(po.data);
            setFaqs(res.data.faqs)
        }

        loadFaqs()
    },[])

  return (
    <View style={styles.container}>     
        <Header navigation={navigation} points={points} />
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.content}>
                {faqs.map(item => (
                    <View key={item.id}>
                        <Text style={{fontSize:18,fontWeight:'bold',margin:10,color:'#3078be'}}>{item.question}</Text>
                        <Text style={{fontSize:14,marginLeft:10,lineHeight:23}}>{item.answer}</Text>
                    </View>
                ))}
            </View>
        </ScrollView>
       
    </View>
  );
}
