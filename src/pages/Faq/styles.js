import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    content:{
        padding:10,
    }
})

export default styles