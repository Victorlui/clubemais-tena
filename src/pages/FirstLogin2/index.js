import React, {useEffect} from 'react';

import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';

import BemVindo from '../../assets/bem.png';
import MeusResgates from '../../assets/meusResgates.png';
import Extratos from '../../assets/extratos.png';
import ResgatarPontos from '../../assets/resgatePontos.png';
import MenuLateral from '../../assets/menuLateral.png';
import Pronto from '../../assets/pronto.png';
import Começar from '../../assets/btnComecar.png';
import Conteudos from '../../assets/conteudos.png';


import SwiperFlatList from 'react-native-swiper-flatlist';


const {width, height} = Dimensions.get('window');

export default function FirstLogin2({navigation}) {
  const user = useSelector(state => state.user);

  async function navigateLevel() {
    navigation.navigate('Level');
  }


  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        {user.user.type === 2 && user.user.store.cluster.name === 'A' && (
          <SwiperFlatList
            index={0}
            showPagination
            paginationActiveColor="#3078be">
            <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={MenuLateral}
                    resizeMode="contain"
                    style={{height: 280}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                      marginTop: 30,
                    }}>
                    Menu Lateral
                  </Text>
                  <View style={{width: 300}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Acesse o menu lateral para consultar as suas recompensas
                      por atingimento dos níveis.
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Também poderá ver o extrato dos seus créditos.
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Lembre-se que ao concluir os níveis você poderá ganhar
                      ainda mais créditos!
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={ResgatarPontos}
                    resizeMode="contain"
                    style={{height: 240}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                      marginTop: 30,
                    }}>
                    Resgatar Créditos
                  </Text>
                  <View style={{width: 320}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Os créditos adicionados pela finalização dos níveis
                      poderão ser resgatados aqui.
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Consulte o seu saldo de créditos disponível e escolha o
                      que deseja resgatar.
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Resgates poderão ser liberados em até 48 horas após a data
                      de solicitação.
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={Extratos}
                    resizeMode="contain"
                    style={{height: 350, width: width}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                    }}>
                    Extratos
                  </Text>
                  <View style={{width: 320}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Acompanhe o extrato de créditos e debitos da sua conta
                      neste menu.
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Os Créditos expiram em 30 dias por isso corra para
                      resgatar os seus antes que expirem.
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={MeusResgates}
                    resizeMode="contain"
                    style={{height: 250}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                    }}>
                    Meus Resgates.
                  </Text>
                  <View style={{width: 320}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Acompanhe também o status dos seus resgates realizados.
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Ao clicar no quadro você terá acesso ao número do voucher,
                      ou também do status do seu pedido.
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={Pronto}
                    resizeMode="contain"
                    style={{height: 320}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                    }}>
                    Pronto
                  </Text>
                  <View style={{width: 300}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Agora que você já conhece o App, vamos começar?
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Clique abaixo e bons aprendizados para você!
                    </Text>
                  </View>

                  <TouchableOpacity
                    style={{marginVertical: 20}}
                    onPress={() => {
                      navigateLevel();
                    }}>
                    <Image
                      source={Começar}
                      resizeMode="contain"
                      style={{height: 50}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </SwiperFlatList>
        )}

        {user.user.type === 2 && user.user.store.cluster.name === 'B' && (
          <SwiperFlatList
            index={0}
            showPagination
            paginationActiveColor="#3078be">
            <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={BemVindo}
                    resizeMode="contain"
                    style={{height: 320,aspectRatio:1}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                      marginTop: 20,
                    }}>
                    Bem-Vindo
                  </Text>
                  <View style={{width: 300}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Na tela inicial você acompanhará a evolução dos níveis de
                      aprendizado. O próximo nível é liberado após a finalização
                      do anterior. A liberação acontecerá em até 72 horas
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      As Estrelas conquistadas irão estimular cada vez mais o
                      seu aprendizado!
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={Conteudos}
                    resizeMode="contain"
                    style={{height: 320,aspectRatio:1}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                      marginTop: 20,
                    }}>
                    Conteúdos
                  </Text>
                  <View style={{width: 300}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Cada conteúdo que você realizar irá acumular Estrelas no
                      App, e claro aprimorar seus conhecimentos nos temas
                      abordados.
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      As Estrelas conquistadas irão estimular cada vez mais o
                      seu aprendizado!
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={Pronto}
                    resizeMode="contain"
                    style={{height: 320,aspectRatio:1}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                    }}>
                    Pronto
                  </Text>
                  <View style={{width: 300}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Agora que você já conhece o App, vamos começar?
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Clique abaixo e bons aprendizados para você!
                    </Text>
                  </View>

                  <TouchableOpacity
                    style={{marginVertical: 20}}
                    onPress={() => {
                      navigateLevel();
                    }}>
                    <Image
                      source={Começar}
                      resizeMode="contain"
                      style={{height: 50}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </SwiperFlatList>
        )}

        {user.user.type === 1 && user.user.store.cluster.name === 'A' && (
          <SwiperFlatList
          index={0}
          showPagination
          paginationActiveColor="#3078be">
             <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={Pronto}
                    resizeMode="contain"
                    style={{height: 320}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                    }}>
                    Pronto
                  </Text>
                  <View style={{width: 300}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Agora que você já conhece o App, vamos começar?
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Clique abaixo e bons aprendizados para você!
                    </Text>
                  </View>

                  <TouchableOpacity
                    style={{marginVertical: 20}}
                    onPress={() => {
                      navigateLevel();
                    }}>
                    <Image
                      source={Começar}
                      resizeMode="contain"
                      style={{height: 50}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </SwiperFlatList>
        )}

    {user.user.type === 1 && user.user.store.cluster.name === 'B' && (
          <SwiperFlatList
          index={0}
          showPagination
          paginationActiveColor="#3078be">
             <View style={[styles.child]}>
              <View>
                <View style={styles.image}>
                  <Image
                    source={Pronto}
                    resizeMode="contain"
                    style={{height: 320}}
                  />
                </View>
                <View style={styles.title}>
                  <Text
                    style={{
                      color: '#2e3192',
                      fontWeight: 'bold',
                      fontSize: 22,
                    }}>
                    Pronto
                  </Text>
                  <View style={{width: 300}}>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Agora que você já conhece o App, vamos começar?
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        color: '#222222',
                        fontSize: 18,
                        marginTop: 10,
                      }}>
                      Clique abaixo e bons aprendizados para você!
                    </Text>
                  </View>

                  <TouchableOpacity
                    style={{marginVertical: 20}}
                    onPress={() => {
                      navigateLevel();
                    }}>
                    <Image
                      source={Começar}
                      resizeMode="contain"
                      style={{height: 50}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </SwiperFlatList>
        )}


      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    alignItems: 'center',
    width: '100%',
    marginTop: 10,
  },
  child: {
    height: height * 0.8,
    width,
    justifyContent: 'center',
  },
  text: {
    fontSize: width * 0.5,
    textAlign: 'center',
  },
});
