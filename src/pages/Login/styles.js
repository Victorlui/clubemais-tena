import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
  
    title: {
      color: '#fff',
      textAlign: 'center',
    },
  
    logo: {
      width: 320,
      height: 150,
      marginLeft:15,
      marginTop:10
    
    },
  
    content: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: 50,
    },
    input: {
      backgroundColor: '#fff',
      borderRadius: 20,
      width: 300,
      marginTop: 10,
      padding: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
    },
    button: {
      width: 200,
      backgroundColor: '#061b59',
      display: 'flex',
      flexDirection: 'row',
      marginTop: 30,
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
    },
  
    login: {
      width: 200,
      backgroundColor: '#0c3296',
      display: 'flex',
      flexDirection: 'row',
      marginTop: 30,
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
    },
  
    icon: {
      marginRight: 20,
    },
  
    textButton: {
      color: '#ffff',
      fontWeight: 'bold',
    },
    textClique: {
      fontSize: 12,
      color: '#ffff',
    },
    terms: {
      color: '#1349ca',
      margin: 10,
    },
  });
  