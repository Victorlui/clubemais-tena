import React, {useState, useEffect, useRef} from 'react';
//Redux
import {useDispatch, useSelector} from 'react-redux';
import {signInRequest} from '../../store/modules/auth/actions';

import Modal from 'react-native-modal';

import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';

//Libs
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Animated,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import LinearGradient from 'react-native-linear-gradient';
import {styles} from './styles';
import {WSnackBar, WToast} from 'react-native-smart-tip';

import Lottie from 'lottie-react-native';
import done from '../../assets/loadingNovo.json';

//Icone
import Icon from 'react-native-vector-icons/FontAwesome5';
//Images
import logo from '../../assets/logo.png';
import moment from 'moment';

export default function Login({navigation}) {
  const dispatch = useDispatch();
  const [cpfPoint, setCpfPoint] = useState('');
  const [password, setPassword] = useState('');
  const passwordRef = useRef();
  const cpfRef = useRef();
  const [loading, setLoading] = useState(false);
  const [offset, setOffset] = useState(new Animated.ValueXY({x: 0, y: 50}));
  const [opacity, setOpacity] = useState(new Animated.Value(0));
  const auth = useSelector(state => state.auth);


  useEffect(() => {
    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 20,
      }),

      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
      }),
    ]).start();


  }, []);

  async function onSubmit(cpfPoint, password) {
    setLoading(true);
    if (cpfPoint === '' && password === '') {
      const snackBarOpts = {
        data: 'Preencha os campos.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);

      return null;
    }
    if (cpfPoint === '') {
      const snackBarOpts = {
        data: 'Preencha o cpf.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);

      return null;
    }
    if (password === '') {
      const snackBarOpts = {
        data: 'Preencha a senha.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);

      return null;
    }

    dispatch(signInRequest(cpfPoint, password));
   

    const snackBarOpts = {
      data: auth.error,
      position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
      duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
      textColor: '#fff',
      backgroundColor: 'red',
    };
    WToast.show(snackBarOpts);
    setPassword('');
    setCpfPoint('');
  }

  return (
    <LinearGradient
      start={{x: 0.5, y: 0.1}}
      end={{x: 0.5, y: 0.8}}
      colors={['#1247c2', '#7dbe4c']}
      locations={[0.5, 0.5]}
      style={styles.container}>
      <StatusBar backgroundColor="#1247c2" barStyle="light-content" />

      <KeyboardAvoidingView
        enabled={Platform.OS === 'ios' && true}
        behavior="padding">
        <Animated.View
          style={[{transform: [{translateY: offset.y}]}, {opacity: opacity}]}>
          <Image style={styles.logo} resizeMode="contain" source={logo} />
          <Text style={styles.title}>VANTAGENS E BENEFÍCIOS EXCLUSIVOS</Text>

          <View accessiv style={styles.content}>
            <TextInputMask
              type={'cpf'}
              style={styles.input}
              value={cpfPoint}
              autoCorrect={false}
              onChangeText={text => {
                setCpfPoint(text);
              }}
              autoCapitalize="none"
              placeholder="CPF"
              returnKeyType="next"
              ref={cpfRef}
              onSubmitEditing={() => {
                passwordRef.current.focus();
              }}
            />
            <TextInput
              style={styles.input}
              placeholder="Password"
              value={password}
              secureTextEntry
              onChangeText={text => {
                setPassword(text);
              }}
              ref={passwordRef}
              returnKeyType="send"
              onSubmitEditing={() => {
                onSubmit(cpfPoint, password);
              }}
            />

            <TouchableOpacity
              onPress={() => {
                navigation.navigate('ResetPassword');
              }}
              style={{
                margin: 5,
                width: '80%',
                alignItems: 'flex-end',
                padding: 5,
              }}>
              <Text style={{color: '#0c3296'}}>Esqueceu a senha ?</Text>
            </TouchableOpacity>

            {auth.loading && (
              <Modal isVisible={true}>
                <View style={{alignItems: 'center'}}>
                  <Lottie
                    source={done}
                    autoPlay
                    loop={true}
                    resizeMode="contain"
                    style={{height: 300, width: 300}}
                  />
                </View>
              </Modal>
            )}

            <TouchableOpacity
              style={styles.login}
              accessible={true}
              accessibilityLabel="Tap me!"
              accessibilityHint="Navigates to the previous screen"
              onPress={() => {
                onSubmit(cpfPoint, password);
              }}>
              <Text style={styles.textButton}>Entrar</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                navigation.navigate('FirstAcess');
              }}>
              <Icon
                style={styles.icon}
                name="unlock-alt"
                size={28}
                color="white"
              />

              <View>
                <Text style={styles.textButton}>Primeiro Acesso</Text>
                <Text style={styles.textClique}>Clique Aqui</Text>
              </View>
            </TouchableOpacity>

            <Text style={styles.terms}>
              Todos os direitos reservados a Essity
            </Text>
          </View>
        </Animated.View>
      </KeyboardAvoidingView>
    </LinearGradient>
  );
}

Login.navigationOptions = {
  header: null,
};
