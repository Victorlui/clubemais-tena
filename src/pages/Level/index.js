import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';

//Libs
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  FlatList,
  ImageBackground,
  BackHandler,
  SafeAreaView,
} from 'react-native';
import Animated from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/AntDesign';
import Lock from 'react-native-vector-icons/FontAwesome';

import Modal from 'react-native-modal';

//actions
import {signOut} from '../../store/modules/auth/actions';

//api
import api from '../../services/api';

//estilos
import styles from './styles';

//imgs
import Seta from '../../assets/seta.png';

//ALERTS
import {WSnackBar, WToast} from 'react-native-smart-tip';

//Components
import Header from '../../components/header';
import Terms from '../../../src/components/Terms/FirstAccess';

export default function Level({navigation}) {
  const [loading, setLoading] = useState(false);
  const [level, setLevel] = useState([]);
  const [atividades, setAtividades] = useState([]);
  const [points, setPoints] = useState();
  const [modal, setModal] = useState(false);
  const [bloqueado, setBloqueado] = useState(false);
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();
  const nivel = navigation.getParam('nivel');
  const fim = JSON.stringify(navigation.getParam('fim'));

  useEffect(() => {
    async function loadLevel() {
      try {
        const res = await api.get('/v1/pointsActivity');
        setPoints(res.data);
      } catch (error) {
        if (error.response.data.message === 'Token has expired') {
          dispatch(signOut());
        }
      }
      setLoading(true);

      const atividade = await api.get('v1/atividades');
      setAtividades(atividade.data.atividades);

      const response = await api.get('v1/niveis');
      setLevel(response.data.niveis);
      setLoading(false);
      if (fim) {
        setModal(true);
      }
    }

    loadLevel();
  }, [nivel]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', function() {
      navigation.navigate('Profile');
      return true;
    });
  }, []);

  
  async function onStartNivel(id) {
    try {
      await api.post(`v1/startNivel?nivel_id=${id.nivel_id}`);
      navigation.navigate('Timeline', {
        id: id.nivel_id,
        atividades: atividades,
      });
    } catch (error) {

      if(error.response.status === 401){
        const toastOpts = {
          data: error.response.data.status,
          textColor: '#fff',
          backgroundColor: 'red',
          duration: WToast.duration.LONG, //1.SHORT 2.LONG
          position: WToast.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        };
        WToast.show(toastOpts);
      }else{
        navigation.navigate('Timeline', {
          id: id.nivel_id,
          atividades: atividades,
        });
      }
    }
  }

  async function onAtividades(id) {
    
      navigation.navigate('Timeline', {
        id: id.nivel_id,
        atividades: atividades,
      });
    
  }

  function reduce(items) {
    const fill = atividades.filter(ele => ele.nivel_id === items.items.id && ele.status === 2);
    const found = fill.filter(ele => ele.atividades_users[0]);
    return found.length === 0 ? <Text>0</Text> : <Text>{found.length}</Text>;
  }

  function mensagemBloqueado() {
    setBloqueado(true);
  }

  function modalBloqueado() {
    return (
      <Modal isVisible={bloqueado}>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: 8,
            padding: 10,
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 18}}>Nível bloqueado aguarde liberação!</Text>

          <TouchableOpacity
            style={{
              backgroundColor: '#DD4647',
              padding: 10,
              borderRadius: 8,
              width: '100%',
              marginTop: 20,
              alignItems: 'center',
            }}
            onPress={() => {
              setBloqueado(false);
            }}>
            <Text style={{color: 'white', fontSize: 18}}>OK</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  function Item(items) {

    return (
      <View style={{flex: 1}} >
        <Terms
          pagekey={'firstaccess'}
          title={'hello world'}
          description={'welcome to my app'}
        />
        <Animated.View style={{zIndex: 1}}>
          {modalBloqueado()}
          <View style={styles.level}>
            <View style={styles.image}>
              <Image
                style={{width: 100, height: 100}}
                resizeMode="contain"
                source={
                  items.items.nivels_users.length === 0
                    ? {
                        uri: `https://tena.ciadetrade.eco.br/${items.items.icon_inactive}`,
                      }
                    : items.items.nivels_users[0].status === 0
                    ? {
                        uri: `https://tena.ciadetrade.eco.br/${items.items.icon_inactive}`,
                      }
                    : {
                        uri: `https://tena.ciadetrade.eco.br/${items.items.icon_active}`,
                      }
                }
              />
              <Text style={styles.fontLevel}>{items.items.name}</Text>
            </View>
            <View style={styles.points}>
              <Text style={styles.textPoint05}>
                {items.items.max_points} Estrelas
              </Text>
              {user.user.type === 1 ? (
                <Text>
                  Atividades:{' '}
                  <Text style={styles.textStep}>
                    {items.items.atividades_count}
                  </Text>
                </Text>
              ) : (
                <Text>
                  Realizados:{' '}
                  {items.items.nivels_users.length === 0 ? (
                    <Text style={styles.textStep}>
                      0 de {items.items.atividades_count}
                    </Text>
                  ) : (
                    
                      <Text style={styles.textStep}>
                        {reduce(items)} de {items.items.atividades_count}
                      </Text>
                    
                  )}
                </Text>
              )}
              {items.items.nivels_users.length === 0 ? (
                <TouchableOpacity
                  style={styles.buttonBlock}
                  disabled
                  onPress={() => {}}>
                  <Lock name="lock" color="white" size={20} />
                  <Text style={{marginLeft: 5, color: 'white'}}>Bloqueado</Text>
                </TouchableOpacity>
              ) : (
                items.items.nivels_users.map(bu =>
                  bu.nivel_id === items.items.id &&
                  bu.status === 1 &&
                  bu.user_id === user.user.id ? (
                    <TouchableOpacity
                      key={bu.id}
                      style={styles.buttonStart}
                      onPress={() => onStartNivel(bu)}>
                      <Lock name="play" color="white" size={20} />
                      <Text style={{marginLeft: 5, color: 'white'}}>
                        Quero começar
                      </Text>
                    </TouchableOpacity>
                  ) : bu.nivel_id === items.items.id &&
                    bu.status === 2 &&
                    bu.user_id === user.user.id ? (
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() => onAtividades(bu)}>
                      <Icon name="like1" color="white" size={20} />
                      <Text style={{marginLeft: 5, color: 'white'}}>
                        Finalizado
                      </Text>
                    </TouchableOpacity>
                  ) : bu.nivel_id === items.items.id &&
                    bu.status === 0 &&
                    bu.user_id === user.user.id ? (
                    <TouchableOpacity
                      style={styles.buttonBlock}
                      onPress={() => mensagemBloqueado()}>
                      <Lock name="lock" color="white" size={20} />
                      <Text style={{marginLeft: 5, color: 'white'}}>
                        Bloqueado
                      </Text>
                    </TouchableOpacity>
                  ) : (
                    bu.nivel_id === items.items.id &&
                    bu.status === 3 &&
                    bu.user_id === user.user.id && (
                      <TouchableOpacity
                        key={bu.id}
                        style={styles.buttonContinuos}
                        onPress={() => onAtividades(bu)}>
                        <Lock name="play" color="white" size={20} />
                        <Text style={{marginLeft: 5, color: 'white'}}>
                          Continuar
                        </Text>
                      </TouchableOpacity>
                    )
                  ),
                )
              )}
            </View>
          </View>

          <View style={{display: 'flex', alignItems: 'center'}}>
            <Image source={Seta} resizeMode="contain" />
          </View>
        </Animated.View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <Header navigation={navigation} points={points} />

        {loading === true ? (
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size={30} color="#3078be" />
          </View>
        ) : (
          <FlatList
            data={level}
            showsVerticalScrollIndicator={false}
            keyExtractor={item => String(item.id)}
            renderItem={({item}) => <Item items={item} />}
          />
        )} 
      </View>
    </SafeAreaView>
  );
}
