import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:"#FFF"
  
    },
    title:{
      fontSize:40,
      textAlign:'center',
      fontWeight:'bold',
      color:'#0c2f84'
    },
    subTitle:{
      textAlign:'center',
      marginTop:10,
    },
    level:{
      display:'flex',
      flexDirection:'row',
      alignItems:'center',
      marginTop:30,
      marginBottom:10
    },
    fontLevel:{
      fontSize:16,
      textAlign:"center",
      color:'#222222',
      fontWeight:'bold'
    },
    fontBlock:{
      fontSize:16,
      color:'#838288'
    },
    image:{
      width:'50%',
     justifyContent:'center',
      alignItems:'center'
    },
    points:{
      
      
    },
    textPoint05:{
      color:'#7ac43a',
      fontWeight:'bold',
      fontSize:20
    },
    textPoint10:{
      color:'#e13c6e',
      fontWeight:'bold',
      fontSize:20
    },
    textStep:{
      fontWeight:'bold',
      fontSize:16
    },
    button:{
      backgroundColor: '#65b28c',
      display: 'flex',
      flexDirection: 'row',
      marginTop: 10,
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
    },
    buttonStart:{
      backgroundColor: '#df396c',
      display: 'flex',
      flexDirection: 'row',
      marginTop: 10,
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
    },
    buttonContinuos:{
      backgroundColor: '#e0bc2c',
      display: 'flex',
      flexDirection: 'row',
      marginTop: 10,
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
    },
    buttonBlock:{
      backgroundColor: '#a6adb9',
      display: 'flex',
      flexDirection: 'row',
      marginTop: 10,
      borderRadius: 20,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
    },
    modal: {
     
      height: 320,
      width: '100%',
      borderRadius: 8,
      alignItems: 'center',
      justifyContent: 'center',
      
    },
    close: {
      backgroundColor:'#ffc211',
      alignItems: 'center',
      width: '50%',
      padding:5,
      borderRadius:8,
      marginTop:10,
    },
    textButton:{
      fontSize:16,
      color:"white",
      fontWeight:'bold'
    }
  
  })

  export default styles