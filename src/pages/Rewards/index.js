import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  Picker,
  Platform
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import Reward from '../../components/rewards';

import Header from '../../components/header';

import IOSPicker from 'react-native-ios-picker';

import api from '../../services/api';

const data = ['a','b','c','d','e','f'];

export default function Rewards({navigation}) {
  const [category, setCategory] = useState([]);
  const [loading, setLoading] = useState(false);
  const [valuePicker, setValuePicker] = useState('')
  const saldo = navigation.getParam('saldo');
  const [points, setPoints] = useState();

  const [sald,setSald] = useState(saldo);
  const [premios,setPremios] = useState([])


  useEffect(() => {
    setLoading(true);
   
    loadRecompensa();
  }, []);

  async function loadRecompensa() {
    const response = await api.get('v1/rewards');
    setCategory(response.data.rewards);
    setPremios(response.data.rewards)
    const res = await api.get('/v1/pointsActivity');
    setPoints(res.data);
    setLoading(false);
  }


  async function getSaldo(){
    const response = await api.get('v1/getSaldo');
    setSald(response.data.saldo)
  }

  async function selectCate(itemValue){
    setValuePicker(itemValue)
    const response = await api.get('v1/rewards');
    const filt = response.data.rewards.filter( r => r.label === itemValue)
   
    if(filt[0].label === itemValue){
      setPremios(filt);
    }
  
      
  }

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <Header navigation={navigation} points={points} />
        <View style={styles.header}>
          <Text style={styles.rescue}>Resgate seus Créditos</Text>

          <View style={styles.points}>
            <Text style={styles.available}>Saldo Disponivel:</Text>
            <Text style={styles.pointsNumber}>R${sald}</Text>
          </View>

         {Platform.OS == 'ios' ? (
           null
         ):(
          <View style={styles.picker}>
          <Picker
                selectedValue={valuePicker}
                style={{height: 50, width: 260}}
                onValueChange={(itemValue, itemIndex) =>
                  selectCate(itemValue)
                }
                >
                <Picker.Item label="Selecione..." value="1" />
                
                {
                  category.map(item => (
                    
                    <Picker.Item label={item.label} value={item.label} />
                  ))
                }
              </Picker> 

              
          </View> 
         )}
        </View>

        <View style={styles.reward}>
          {loading === true ? (
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <ActivityIndicator color="blue" size={20} />
            </View>
          ) : (
            <FlatList
              data={premios}
              renderItem={({item}) => <Reward itens={item} onPress={()=>getSaldo()} />}
              keyExtractor={(item, index) => item.id}
            />
          )}
        </View>
      </View>
    </SafeAreaView>
  );
}

Rewards.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    marginTop: 40,
  },
  points: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  pointsNumber: {
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: 5,
  },
  rescue: {
    fontSize: 20,
    color: '#001977',
    fontWeight: 'bold',
  },
  available: {
    color: '#5a5a5a',
  },
  reward: {
    flex: 1,
  },
  picker: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    marginTop: 10,
    width: 30,
    height: 40,
    borderRadius: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: '#d3d3d3',
    width: '80%',
  },
});
