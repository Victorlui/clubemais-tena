import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';

import Logo from '../../assets/logo1.png'

const Offilne = () => {
  return (
    <View style={styles.container}>
        <Image source={Logo} resizeMode="contain" style={{width:250,height:250}} />
        <Text style={styles.title}>Voce está offline?</Text>
        <Text style={styles.Subtitle}>Conecte-se na internet e tente novamente</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:"center"
  },
  title:{
    margin:10,
    color: '#2F3888',
    fontSize:18,
    fontWeight:'bold'
  },
  Subtitle:{
    margin:10,
    color: '#2F3888',
    fontSize:12
  }
});



export default Offilne;