import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    
  },

  header: {
   
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
  },

  body: {
   
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
  },

  title: {
    marginTop: -30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },

  subTitle: {
    fontSize: 16,
    color: '#fff',
    marginBottom: 20,
  },

  logo: {
    width: 250,
    height: 190,
    resizeMode:'contain',
    marginBottom:10
   
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  input: {
    backgroundColor: '#d3d3d3',
    borderRadius: 8,
    borderWidth:1,
    borderColor:'#d3d3d3',
    width: '80%',
    marginTop: 20,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 6},
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 10,
    marginLeft:30
  },
  button: {
    width: 200,
    backgroundColor: '#0b2f94',
    marginTop: 30,
    borderRadius: 20,
    alignItems: 'center',
    padding: 10,
    borderWidth: 1,
    borderColor: '#fff',
    marginBottom: 20,
    elevation:5
  },

  buttonLogut: {
    width: 200,
    backgroundColor: '#f00',
    marginTop: 2,
    borderRadius: 20,
    alignItems: 'center',
    padding: 10,
    borderWidth: 1,
    borderColor: '#fff',
    marginBottom: 20,
    elevation:5
  },

  textButton: {
    color: '#ffff',
    fontWeight: 'bold',
  },
  terms: {
    color: '#1349ca',
    marginTop: 40,
  },
});

export default styles;
