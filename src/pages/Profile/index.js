import React, {useState, useEffect, useRef} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
  Animated,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView
} from 'react-native';

import {
  Card,
  CardItem,
  Body,
  Content
} from 'native-base';

import {signOut} from '../../store/modules/auth/actions';
import {UpdadeSuccess} from '../../store/modules/user/actions';

import {TextInputMask} from 'react-native-masked-text';
import {WSnackBar, WToast} from 'react-native-smart-tip';

import styles from './styles';
import api from '../../services/api';
import Header from '../../components/header';

export default function Profile({navigation}) {
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [cell, setCell] = useState('');
  const [password,setPassword] = useState('');
  const [confirmPassword,setConfirmPassword] = useState('')
  const [loading, setLoading] = useState(false);
  const [offset, setOffset] = useState(new Animated.ValueXY({x: 0, y: 50}));
  const [opacity, setOpacity] = useState(new Animated.Value(0));
  const [userEdit, setUserEdit] = useState({});
  const [points, setPoints] = useState();
  const user = useSelector(state => state.user);
  const auth = useSelector(state => state.auth);

  useEffect(() => {
    async function loadUserRecomend() {
      const res = await api.get('/v1/pointsActivity');
      setPoints(res.data);
    }

    loadUserRecomend();

    if (user.userEdit === null) {
      setName(user.user.name);
      setEmail(user.user.email);
      setCell(user.user.celular);
    } else {
      setName(user.userEdit.name);
      setEmail(user.userEdit.email);
      setCell(user.userEdit.celular);
    }

    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 20,
      }),

      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
      }),
    ]).start();
  }, []);


  async function onSubmit() {
    setLoading(true);

    if(password !== confirmPassword){
      Alert.alert(
        "Senha não conferem",
        "Senha digitadas não conferem, Por favor informe outra senha.",
        [
          { text: "OK", onPress: () => {} }
        ],
        { cancelable: false }
      );
    }if(password !== '' && confirmPassword !== '') {
       await api.post('v1/editUser', {
        email: email,
        password:password
      });
      Alert.alert(
        "Senha atualizada",
        "Senha atualizada com sucesso.",
        [
          { text: "OK", onPress: () => {} }
        ],
        { cancelable: false }
      );
      setPassword('')
      setConfirmPassword('')
      
    }else{
      const response = await api.post('v1/editUser', {
        name: name,
        email: email,
        celular: cell,
      });
  
      const users = {
        id: user.user.id,
        store_id: user.user.store_id,
        name: response.data.user.name,
        cpf: user.user.cpf,
        type: user.user.type,
        email: response.data.user.email,
        aceite_reg: user.user.aceite_reg,
        email_verified_at: user.user.email_verified_at,
        celular: response.data.user.celular,
        status:user.user.type,
        firstTime: auth.dados.firstTime,
        store: {
          id: user.user.store.id,
          name:user.user.store.name,
          cnpj:user.user.store.cnpj,
          status: user.user.store.status,
          cluster_id: user.user.store.cluster_id,
          vacancies: user.user.store.vacancies,
          cluster:{
            id: user.user.store.cluster.id,
            name:user.user.store.cluster.name,
            haveGift:user.user.store.cluster.haveGift,
            created_at: user.user.store.cluster.created_at,
            updated_at: user.user.store.cluster.updated_at
          }
        }
      };
  
      dispatch(UpdadeSuccess(users));
    }

    setLoading(false);
  }

  function FormaterCnpj(cnpj) {
    const cnpjFormater = cnpj.replace(
      /^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/,
      '$1.$2.$3/$4-$5',
    );

    return (
      <Text style={{fontSize: 16, color: '#222222', marginTop: 10}}>
        {cnpjFormater}
      </Text>
    );
  }

  return (
   <SafeAreaView style={{flex:1}}>
      
      <Header
        navigation={navigation}
        nome={userEdit}
        points={points}
      />
      <ScrollView style={styles.container}>
      <KeyboardAvoidingView
        enabled={Platform.OS === 'ios' && true}
        behavior="padding">
        <Animated.View
          style={[{transform: [{translateY: offset.y}]}, {opacity: opacity}]}>
          <View style={styles.body}>
          
          <Card style={{width:'80%', marginTop:20}}>
            <CardItem>
              <Body style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
              <View style={{flex:1,alignItems:'center', alignContent:"center", textAlign: 'center'}}>  
                  <Text style={{textAlign: 'center', fontSize: 16, color: '#222222', marginTop: 10, fontWeight:"bold" }}>
                    Loja:
                  </Text>    
                  <Text style={{textAlign: 'center', fontSize: 16, color: '#222222', marginTop: 10}}>
                    {user.user.store.name}
                  </Text>
                  <Text style={{textAlign: 'center', fontSize: 16, color: '#222222', marginTop: 10, fontWeight:"bold" }}>
                    CNPJ:
                  </Text>    
                {FormaterCnpj(user.user.store.cnpj)}
                </View>
              </Body>
            </CardItem>
          </Card>
        

            <View
              style={{
                margin: 20,
                width: '80%',
               
               
              }}>
              
              <TextInput
                style={{
                  backgroundColor: '#fff',
                  borderRadius: 8,
                  borderWidth: 1,
                  borderColor: '#d3d3d3',
                  height: 40,
                  padding: 5,
                  textAlign:"center"
                }}
                placeholder="Nome"
                value={name}
                onChangeText={text => setName(text)}
                autoCorrect={false}
              />
            </View>

            <View style={{margin: 20, width: '80%'}}>
             
              <TextInput
                style={{
                  backgroundColor: '#fff',
                  borderRadius: 8,
                  borderWidth: 1,
                  borderColor: '#d3d3d3',
                  height: 40,
                  padding: 5,
                  textAlign:"center"
                }}
                placeholder="E-mail"
                value={email}
                onChangeText={text => setEmail(text)}
              />
            </View>

            <View style={{margin: 20, width: '80%'}}>
              
              <TextInputMask
                type={'cel-phone'}
                options={{
                  maskType: 'BRL',
                  withDDD: true,
                  dddMask: '(99) ',
                }}
                style={{
                  backgroundColor: '#fff',
                  borderRadius: 8,
                  borderWidth: 1,
                  borderColor: '#d3d3d3',
                  height: 40,
                  padding: 5,
                  textAlign:"center"
                }}
                value={cell}
                onChangeText={text => {
                  setCell(text);
                }}
                placeholder="Celular"
                returnKeyType="next"
              />
            </View>

            <View style={{margin: 20, width: '80%'}}>
              
              <TextInput
              
                secureTextEntry={true}
                style={{
                  backgroundColor: '#fff',
                  borderRadius: 8,
                  borderWidth: 1,
                  borderColor: '#d3d3d3',
                  height: 40,
                  padding: 5,
                  textAlign:"center"
                }}
                value={password}
                onChangeText={text => {
                  setPassword(text);
                }}
                placeholder="Nova senha"
               
              />
            </View>

            <View style={{margin: 20, width: '80%'}}>
              
              <TextInput
                
                secureTextEntry={true}
                style={{
                  backgroundColor: '#fff',
                  borderRadius: 8,
                  borderWidth: 1,
                  borderColor: '#d3d3d3',
                  height: 40,
                  padding: 5,
                  textAlign:"center"
                }}
                value={confirmPassword}
                onChangeText={text => {
                  setConfirmPassword(text);
                }}
                placeholder="Confirmar senha"
               
              />
            </View>

            {loading ? (
              <View style={{width: '100%', height: 300}}>
                <ActivityIndicator
                  size={30}
                  style={{top: 20}}
                  color="#3078be"
                />
              </View>
            ) : (
              <>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => {
                    onSubmit();
                  }}>
                  <Text style={styles.textButton}>ATUALIZAR</Text>
                </TouchableOpacity>
                {/* <TouchableOpacity
                  style={styles.buttonLogut}
                  onPress={() => {
                    handleLogout();
                  }}>
                  <Text style={styles.textButton}>SAIR</Text>
                </TouchableOpacity> */}
              </>
            )}
          </View>
        </Animated.View>
      </KeyboardAvoidingView>
    </ScrollView>
   </SafeAreaView>
  );
}

Profile.navigationOptions = {
  header: null,
};
