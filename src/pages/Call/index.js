import React, {useEffect, useState, useRef} from 'react';

import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';

import Header from '../../components/header';

import api from '../../services/api';

import {WSnackBar, WToast} from 'react-native-smart-tip';

export default function Call({navigation}) {
  const [points, setPoints] = useState();
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');
  const messageRef = useRef();

  useEffect(() => {
    async function pontos() {
      const res = await api.get('/v1/pointsActivity');
      setPoints(res.data);
    }

    pontos();
  }, []);

  async function onSubmitMessage() {
    setLoading(true);
    
    if (message === '') {
      const snackBarOpts = {
        data: 'Digite sua Mensagem.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
    } 
      
    try{
      const response = await api.post(`v1/contato?mensagem=${message}`);
      const snackBarOpts = {
        data: response.data.mensagem,
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'green',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
      setMessage('');
    }catch(error){

    }

  }

  return (
    <SafeAreaView style={{flex:1}}>
      <ScrollView>
      <View style={styles.container}>
        <Header navigation={navigation} points={points} />
        <View style={styles.content}>
          <View style={styles.input} >
            <TextInput
              multiline={true} 
              blurOnSubmit={true}
              numberOfLines={10}
              value={message}
              style={{textAlignVertical:'top'}}
              placeholder="Digite sua mensagem"
              returnKeyType="done"
              ref={messageRef}
              onChangeText={text => setMessage(text)}
              onSubmitEditing={() => {
                onSubmitMessage()
              }}
            />
          </View>
          {loading === true ? (
            <ActivityIndicator
              size={30}
              color="#3078be"
              style={{marginTop: 10}}
            />
          ) : (
            <TouchableOpacity
              onPress={() => onSubmitMessage()}
              style={styles.button}>
              <Text style={{color: 'white'}}>Enviar</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
      </ScrollView>
     
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    marginTop: 30,
  },
  input: {
    borderWidth: 1,
    borderColor: '#d3d3d3',
    borderRadius: 8,
    height: 200,
    width: 300,
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#3078be',
    width: 300,
    alignItems: 'center',
    borderRadius: 8,
    padding: 10,
    marginTop: 10,
  },
});
