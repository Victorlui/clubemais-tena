import React, {useState, useEffect} from 'react';

//Icon 
import Icon from 'react-native-vector-icons/FontAwesome'

//Modal
import Modal from 'react-native-modal';

//api
import api from '../../services/api';

//libs
import {
  View,
  Text,
  Animated,
  ScrollView,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';

//estilos
import styles from './styles';

//Components
import Header from '../../components/header';

export default function MyRescues({navigation}) {
  const [offset, setOffset] = useState(new Animated.ValueXY({x: 0, y: 50}));
  const [opacity, setOpacity] = useState(new Animated.Value(0));
  const resgates = navigation.getParam('resgates');
  const [points, setPoints] = useState();
  const [openModal, setOpenModal] = useState(false)

  useEffect(() => {
    async function loadUserRecomend() {
      const res = await api.get('/v1/pointsActivity');
      setPoints(res.data);
    }

    loadUserRecomend();
    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 20,
      }),

      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
      }),
    ]).start();
  }, []);

  function onModalVouche(voucher){
   if(voucher){
    setOpenModal(true)
   }
  }

  function modalVoucher(voucher,observation){
   
    return(
      <Modal isVisible={openModal}>
        <View style={{backgroundColor:'white',borderRadius:8,alignItems:'center',margin:10,padding:10}}>
          <Text style={{fontWeight:'bold',fontSize:18,marginVertical:10,color:"#222222"}}>Voucher</Text>
          <Icon name="ticket" color="#AD3626" size={60} style={{marginVertical:10}} />
          <Text style={{fontSize:18,fontWeight:'bold',marginVertical:10,color:"#222222"}}>{voucher}</Text>
          {observation !== null && (
              <View style={{padding:10,borderRadius:5,borderWidth:1,margin:10,width:'100%',borderColor:'#D3D3D3'}}>
              <Text>teste</Text>
            </View>
          )}
          <View style={{borderTopWidth:0.7,borderColor:'gray',width:'100%',alignItems:'center',justifyContent:'center'}}>
            <TouchableOpacity style={{marginVertical:5}} onPress={()=>{setOpenModal(false)}}>
              <Text style={{fontSize:16,color:'#3DB994'}}>Copiar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    ) 
  }

  return (
    <SafeAreaView>
      <ScrollView>
     
     <View style={styles.container}>
       <Header navigation={navigation} points={points} />
       <View style={styles.header}>
         <Text style={styles.rescue}>Meus Resgates</Text>
       </View>

       <View style={styles.reward}>
         {resgates.length === 0 ? (
           <View style={{alignItems: 'center', marginTop: 30}}>
             <Text style={{fontSize: 18, color: '#222222'}}>
               Nenhum resgate feito.
             </Text>
           </View>
         ) : (
           resgates.map(items => (
             <>
              {modalVoucher(items.voucher,items.observation)}
               <Animated.View
                 key={items.id}
                 style={[
                   {transform: [{translateY: offset.y}]},
                   {opacity: opacity},
                 ]}>
                 <TouchableOpacity
                   onPress={() => onModalVouche(items.voucher)}
                   style={{
                     backgroundColor:
                       items.status === 0
                         ? '#3078be'
                         : items.status === 1
                         ? '#ED9D02'
                         : items.status === 2 && '#069019',
                     display: 'flex',
                     alignItems: 'center',
                     justifyContent: 'space-between',
                     borderRadius: 10,
                     padding: 15,
                     paddingLeft: 10,
                     paddingRight: 10,
                     margin: 10,
                   }}>
                   <View style={styles.boxRescue}>
                     <View style={styles.nivel}>
                       <Text style={styles.textDebito}>
                         {items.reward.details}
                       </Text>
                     </View>

                     <View style={{alignItems: 'center'}}>
                       <Text style={{fontSize: 20, color: 'white'}}>
                         Valor
                       </Text>
                       <Text style={styles.textDebito}>R$ {items.value}</Text>
                     </View>
                   </View>
                   {items.status === 0 && (
                     <View
                       style={{
                         borderTopWidth: 0.5,
                         borderTopColor: 'white',
                         width: '100%',
                         alignItems: 'center',
                       }}>
                       <Text
                         style={{marginTop: 10, fontSize: 18, color: 'white'}}>
                         Solicitado
                       </Text>
                     </View>
                   )}

                   {items.status === 1 && (
                     <View
                       style={{
                         borderTopWidth: 0.5,
                         borderTopColor: 'white',
                         width: '100%',
                         alignItems: 'center',
                       }}>
                       <Text  style={{marginTop: 10, fontSize: 18, color: 'white'}}>Processando</Text>
                     </View>
                   )}

                   {items.status === 2 && (
                     <View
                       style={{
                         borderTopWidth: 0.5,
                         borderTopColor: 'white',
                         width: '100%',
                         alignItems: 'center',
                       }}>
                       <Text  style={{marginTop: 10, fontSize: 18, color: 'white'}}>Resgatado</Text>
                     </View>
                   )}
                 </TouchableOpacity>
               </Animated.View>
             </>
           ))
         )}
       </View>
     </View>
   </ScrollView>
    </SafeAreaView>
  );
}

MyRescues.navigationOptions = {
  header: null,
};
