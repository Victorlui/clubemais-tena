// PROGRESS BAR
import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  StyleSheet,
  Dimensions,
  Button,
  Alert,
  Text,
} from 'react-native';
 
import ProgressBarAnimated from 'react-native-progress-bar-animated';

// STAR COMPONENT
import StarProgress from './StarProgress';
 
export default class ProgressBar extends React.Component {
 
  constructor(props){
    super(props);
    
  }
 
  state = {
    progressWithOnComplete: 0,
  }
  
 
  increase = (key, value) => {
    this.setState({
      [key]: this.state[key] + value,
    });
  }

  componentDidMount(){
    const pontos = this.props.pontos
   
    const porcentage = pontos / 100
    const strin = porcentage.toString();
    const converte =strin.replace(/^5+./, '')

    this.increase('progressWithOnComplete',pontos)

  

   
  }
 
  render() {
    const barWidth = Dimensions.get('screen').width - 30;
 
    return (
      
      <View style={{flex:1}}>
      <StarProgress pontos={this.props.pontos} />
      <ProgressBarAnimated
        width={barWidth}
        value={this.state.progressWithOnComplete}
        backgroundColorOnComplete="green"
        barEasing="bounce"
        backgroundAnimationDuration={4000}
      />
    
    </View>
          
    )}}

    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: '#FFF',
        marginTop: 50,
        padding: 15,
      },
      buttonContainer: {
        marginTop: 15,
      },
      label: {
        color: '#999',
        fontSize: 14,
        fontWeight: '500',
        marginBottom: 10,
      },
    });
          