import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';

// STAR ICONE
import Star from 'react-native-vector-icons/AntDesign';

export default class StarProgress extends Component {
  render() {
    
    return (
      <View style={styles.container}>

        {this.props.pontos === 50 && (
          <Star name="star" color="#FBCC00" size={30} />
        )}

        {this.props.pontos === 100 && (
          <>
            <Star name="star" color="#FBCC00" size={30} />
            <Star name="star" color="#FBCC00" size={30} />
          </>
        )}

        {this.props.pontos === 150 && (
          <>
            <Star name="star" color="#FBCC00" size={30} />
            <Star name="star" color="#FBCC00" size={30} />
            <Star name="star" color="#FBCC00" size={30} />
          </>
        )}

        {this.props.pontos === 200 && (
          <>
            <Star name="star" color="#FBCC00" size={30} />
            <Star name="star" color="#FBCC00" size={30} />
            <Star name="star" color="#FBCC00" size={30} />
            <Star name="star" color="#FBCC00" size={30} />
          </>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
    padding: 10,
  },
});
