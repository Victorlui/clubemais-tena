import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';

//REACT-NATIVE UI
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TextInput,
  ToastAndroid,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';

//ICONES
import Icon from 'react-native-vector-icons/AntDesign';
import Fa from 'react-native-vector-icons/FontAwesome';
import Add from 'react-native-vector-icons/Ionicons';

//MODAL
import Modal from 'react-native-modal';

//ALERTS
import {WSnackBar, WToast} from 'react-native-smart-tip';

// PROGRESS BAR COMPONENT
import ProgressBar from './ProgressBar';

//COMPONENTS
import Header from '../../components/header';

//SERVICES
import api from '../../services/api';

//ESTILOS
import {styles} from './styles';


export default function Recommendation({navigation}) {
  const [modalDelete, setModalDelete] = useState(false);
  const [modal, setModal] = useState(false);
  const [idUserdelete,setIdUserDelete] = useState(0);
  const [cpf, setCpf] = useState('');
  const [email, setEmail] = useState('');
  const [phone,setPhone] = useState('')
  const [userRecomend, setUserRecomend] = useState([]);
  const [loading, setLoading] = useState(false);
  const [vacancy, setVacancy] = useState('');
  const auth = useSelector(state => state.auth);
  const saldo = navigation.getParam('saldo');
  const [points, setPoints] = useState();

  useEffect(() => {
    async function loadUserRecomend() {
      const res = await api.get('/v1/pointsActivity');
      setPoints(res.data);
      const response = await api.get('v1/recommends');
      setUserRecomend(response.data.user);
      setVacancy(response.data.vacancy);
    }

    loadUserRecomend();
  }, []);

  function openModal() {
    setModal(true);
  }

  function closemodal() {
    setModal(false);
  }

  async function onDeleteRecomend(id) {
    try {
      await api.post(`v1/delRecommended?recommend_id=${id}`);
      const response = await api.get('v1/recommends');
      setUserRecomend(response.data.user);
     
      const snackBarOpts = {
        data: 'Usuário deletado',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.SHORT, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: '#04911B',
      };
      WToast.show(snackBarOpts);
      setModalDelete(false)
    } catch (error) {
      setModalDelete(false)
      const snackBarOpts = {
        data: error.response.data.mensagem,
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.SHORT, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
    }
  }

  async function onSubmit(cpf, email,phone) {
    setLoading(true);

    if (cpf === '' && email === '') {
      ToastAndroid.show('Preencha os campos', ToastAndroid.TOP);
      setLoading(false);
      return null;
    }

    if (cpf === '') {
      ToastAndroid.show('Preencha o CPF', ToastAndroid.TOP);
      setLoading(false);
      return null;
    }

    if (email === '') {
      ToastAndroid.show('Preencha o email', ToastAndroid.TOP);
      setLoading(false);
      return null;
    }

    const cpf1 = cpf.replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, '');
    const tel = phone.replace(/[^\d]+/g, '');

    try {
      await api.post(
        `https://tena.ciadetrade.eco.br/api/v1/recommend?cpf=${cpf1}`,
        {
          email: email,
          phone: tel
        },
      );
      setEmail('');
      setCpf('');
      setPhone('')

      const snackBarOpts = {
        data: 'Funcionário Indicado.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.SHORT, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: '#04911B',
      };
      WToast.show(snackBarOpts);
      const response = await api.get('v1/recommends');
      setUserRecomend(response.data.user);
      closemodal();
      setLoading(false);
    } catch (error) {
      const toastOpts = {
        data: error.response.data.status,
        textColor: '#fff',
        backgroundColor: 'red',
        duration: WToast.duration.LONG, //1.SHORT 2.LONG
        position: WToast.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
      };
      setEmail('');
      setCpf('');
      setPhone('')
      WToast.show(toastOpts);
      setLoading(false);
      closemodal();
    }
  }

  function formatCpf(cpf) {
    const cpf1 = cpf.replace(/[^\d]/g, '');
    const cpfFormat = cpf1.replace(
      /(\d{3})(\d{3})(\d{3})(\d{2})/,
      '$1.$2.$3-$4',
    );

    return <Text style={styles.title}>{cpfFormat}</Text>;
  }

  function addBalconista() {
    return (
      <Modal animationIn="fadeInLeft" isVisible={modal} onBackdropPress={()=>setModal(false)}>
        <View style={styles.modal}>
          <Text style={{fontWeight: 'bold', fontSize: 18}}>
            Adicionar balconista
          </Text>
          <TextInputMask
            type={'cpf'}
            value={cpf}
            style={styles.input}
            onChangeText={text => {
              setCpf(text);
            }}
            placeholder="CPF"
          />
          <TextInput
            style={styles.input}
            value={email}
            onChangeText={text => {
              setEmail(text);
            }}
            autoCapitalize="none"
            placeholder="E-mail"
            keyboardType="email-address"
          />

      <TextInputMask
            type={'cel-phone'}
            options={{
              maskType: 'BRL',
              withDDD: true,
              dddMask: '(99) '
            }}
            value={phone}
            style={styles.input}
            onChangeText={text => {
              setPhone(text);
            }}
            placeholder="Telefone"
          />

          <View style={styles.buttonModal}>
            {loading === true ? (
              <ActivityIndicator
                color="#3078be"
                size={20}
                style={{marginTop: 30}}
              />
            ) : (
              <>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => onSubmit(cpf, email,phone)}>
                  <Text style={{color: '#fff', fontWeight: 'bold'}}>
                    Adicionar
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.button}
                  onPress={() => {
                    closemodal();
                  }}>
                  <Text style={{color: '#fff', fontWeight: 'bold'}}>
                    Cancelar
                  </Text>
                </TouchableOpacity>
              </>
            )}
          </View>
        </View>
      </Modal>
    );
  }

  function deleteBalconista() {
    return (
      <Modal isVisible={modalDelete} onBackdropPress={()=> setModalDelete(false)}>
        <View style={{backgroundColor: 'white', borderRadius: 8, padding: 10}}>
          <View style={{alignItems: 'center', marginVertical: 10}}>
            <Text style={{textAlign: 'center'}}>
              Você deseja deletar um usuário?
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                padding: 10,
                borderWidth: 0.5,
                borderColor: '#d3d3d3',
                width: '45%',
                alignItems: 'center',
                borderRadius: 8,
              }}>
              <TouchableOpacity
              onPress={()=>{onDeleteRecomend(idUserdelete)}}
                hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
                <Text style={{color: 'red'}}>Deletar</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                padding: 10,
                borderWidth: 0.5,
                borderColor: '#d3d3d3',
                width: '45%',
                alignItems: 'center',
                borderRadius: 8,
              }}>
              <TouchableOpacity
                onPress={() => {
                  setModalDelete(false);
                }}>
                <Text>Cancelar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  function openDeleteUser(id){
    setModalDelete(true)
    setIdUserDelete(id)
  }

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView style={styles.container}>
        {addBalconista()}
        {deleteBalconista()}
        <Header navigation={navigation} points={points} />

        <View style={styles.evolution}>
          <View style={styles.header}>
            <Text style={{color: '#fff', fontSize: 12, fontWeight: 'bold'}}>
              NIVEL DE
            </Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 20,
                  fontWeight: 'bold',
                  fontStyle: 'italic',
                }}>
                EVOLUÇÃO
              </Text>
              <Text
                style={{
                  color: '#1be4ff',
                  fontSize: 18,
                  fontWeight: 'bold',
                  marginLeft: 10,
                }}>
                do meu PDV
              </Text>
            </View>
          </View>

          {/* PROGRESS BAR */}
          <View style={styles.body}>
            <ProgressBar pontos={auth.dados.points} />
          </View>
          {/*FIM DO PROGRESS BAR */}

          <View style={styles.footer}>
            <View style={styles.warning}>
              <View style={{width: '20%', alignItems: 'center'}}>
                <Icon name="warning" size={28} />
              </View>

              <View style={{width: '80%'}}>
                <Text>
                  Conquiste estrelas com engajamento do seus funcionários nos
                  treinamentos.
                </Text>
              </View>
            </View>

            <View style={styles.warning}>
              <Text style={styles.title}>Total de Pontos Funcionários:</Text>
              <Text style={styles.textPeople}>{auth.dados.points}</Text>
            </View>
          </View>
        </View>

        <View>
          {userRecomend.length === 0 ? (
            <View style={{alignItems: 'center', marginVertical: 10}}>
              <Text style={{color: '#3078be'}}>Sem indicados no momento.</Text>
            </View>
          ) : (
            userRecomend.map(recom =>
              recom.users === null ? (
                <View key={recom.id} style={styles.evolution}>
                  <View style={styles.people}>
                    {formatCpf(recom.cpf)}
                    <Text>Balconista</Text>

                    <View>
                      <Text style={{fontSize: 16, marginTop: 10}}>
                        {recom.email}
                      </Text>
                    </View>

                    <View style={styles.level}>
                      <View style={styles.content}>
                        <Text style={styles.textPeopleLevel}>Nível:</Text>
                        <Text style={{fontSize: 16}}>Iniciante</Text>
                      </View>
                      <View style={styles.content}></View>
                    </View>

                    <View style={styles.level}>
                        <View style={styles.content}>
                          <Text style={styles.textPeopleLevel}>Status:</Text>

                          {recom.status === 0 && (
                            <Text style={{fontSize: 16}}>Indicado</Text>
                          )}

                          {recom.status === 1 && (
                            <Text style={{fontSize: 16}}>Cadastrado</Text>
                          )}

                          {recom.status === 2 && (
                            <Text style={{fontSize: 16}}>Indicado com auditoria</Text>
                          )}
                        </View>
                        <View style={styles.content}></View>
                      </View>
                    </View>

                  <View style={styles.footerPeople}>
                    <TouchableOpacity
                      onPress={() => {
                        openDeleteUser(recom.id)
                      }}
                      style={styles.icon}>
                      <Icon name="closecircle" color="red" size={22} />
                      <Text style={styles.delete}>Excluir</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              ) : (
                <View key={recom.users.id} style={styles.evolution}>
                <View style={styles.people}>
                  <Text style={styles.title}>{recom.users.name}</Text>
                  <Text>Balconista</Text>

                  <View>
                    <Text style={{fontSize: 16, marginTop: 10}}>
                      {recom.users.email}
                    </Text>
                  </View>

                  <View style={styles.level}>
                    <View style={styles.content}>
                      <Text style={styles.textPeopleLevel}>Nível:</Text>
                      {recom.users.nivels_users.length === 0 ? (
                        <Text style={{fontSize: 16}}>Iniciante</Text>
                      ) : (
                        recom.users.nivels_users.map(nivel =>
                          nivel.nivel_id === 1 ? (
                            <Text style={{fontSize: 16}}>Iniciante</Text>
                          ) : nivel.nivel_id === 2 ? (
                            <Text style={{fontSize: 16}}>Entusiasta</Text>
                          ) : nivel.nivel_id === 3 ? (
                            <Text style={{fontSize: 16}}>
                              Intermediário
                            </Text>
                          ) : nivel.nivel_id === 4 ? (
                            <Text style={{fontSize: 16}}>Expert</Text>
                          ) : (
                            nivel.nivel_id === 1 && (
                              <Text style={{fontSize: 16}}>Genial</Text>
                            )
                          ),
                        )
                      )}
                    </View>
                    <View style={styles.content}></View>
                  </View>

                  <View style={styles.level}>
                    <View style={styles.content}>
                      <Text style={styles.textPeopleLevel}>Status:</Text>

                      {recom.users.status === 0 && (
                        <Text style={{fontSize: 16}}>Indicado</Text>
                      )}

                      {recom.users.status === 1 && (
                        <Text style={{fontSize: 16}}>Cadastrado</Text>
                      )}

                      {recom.users.status === 2 && (
                        <Text style={{fontSize: 16}}>Indicado com auditoria</Text>
                      )}
                    </View>
                    <View style={styles.content}></View>
                  </View>
                </View>

                <View style={styles.footerPeople}>
                  <TouchableOpacity
                    onPress={() => {
                      openDeleteUser(recom.users.id)
                    }}
                    style={styles.icon}>
                    <Icon name="closecircle" color="red" size={22} />
                    <Text style={styles.delete}>Excluir</Text>
                  </TouchableOpacity>
                </View>
              </View>
              ),
            )
          )}
        </View>

        <View style={styles.evolution}>
          <TouchableOpacity
            style={styles.add}
            onPress={() => {
              openModal();
            }}>
            <Add name="md-add-circle-outline" color="#7a7a7a" size={50} />
            <Text>Adicionar mais usuários</Text>
            <Text>Você poderá indicar até {vacancy} pessoas</Text>
          </TouchableOpacity>
        </View>

        <Text style={styles.politic}>Todos os direitos reservado a Essity</Text>
      </ScrollView>
    </SafeAreaView>
  );
}
