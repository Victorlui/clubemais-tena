import React, {Component} from 'react';  
import {Platform, StyleSheet, Text, View, Animated} from 'react-native';  
  
export default class ProgressBarDois extends Component {  
    state={  
        progressStatus: 0,  
    }  
    anim = new Animated.Value(0);  
    componentDidMount(){  
        this.onAnimate();  
    }  
    onAnimate = () =>{  
        this.anim.addListener(({value})=> {  
            this.setState({progressStatus: parseInt(value,10)});  
        });  
        Animated.timing(this.anim,{  
             toValue: 100,  
             duration: 5000,  
        }).start();  
    }  

    componentDidUpdate() {
      let acao = this.state.progressStatus;
      if (acao == "100") {
        alert('ola')
      }
    }
  render() {  
    return (  
      <View style={styles.container}>  
            <Animated.View  
                style={[  
                    styles.inner,{width: this.state.progressStatus +"%",
                    backgroundColor: (this.state.progressStatus=="100"?'green':'#148cF0')
                  },  
                ]}  
            />  
            <Animated.Text style={styles.label}>  
                    <Text>progresso</Text>  
            </Animated.Text>  
      </View>  
    );  
  }  
}  
const styles = StyleSheet.create({  
    container: {  
    width: "100%",  
    height: 35,  
    padding: 3,  
    borderColor: "#333",  
    borderWidth: 1,  
    borderRadius: 30,  
    marginTop: 20,  
    justifyContent: "center",  
  },  
  inner:{  
    width: "100%",  
    height: 30,  
    borderRadius: 15,  
    backgroundColor:"green",  
  },  
  label:{  
    fontSize:23,  
    color: "white",  
    position: "absolute",  
    zIndex: 1,  
    alignSelf: "center",  
  }  
});