import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import api from '../../services/api';

import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  Animated,
  ActivityIndicator,
} from 'react-native';

import Back from 'react-native-vector-icons/Ionicons';
import styles from './styles';

import Modal from 'react-native-modal';
import Header from '../../components/header';

import Posts from '../../components/timeline';

import Lottie from 'lottie-react-native';

import Winner from '../../assets/trophy.json';
import {signOut} from '../../store/modules/auth/actions';

export default function Timeline({navigation}) {
  const [offset, setOffset] = useState(new Animated.ValueXY({x: 0, y: 50}));
  const [opacity, setOpacity] = useState(new Animated.Value(0));
  const [loading, setLoading] = useState(false);
  const [level, setLevel] = useState([]);
  const [like, setLike] = useState(0);
  const [modal, setModal] = useState(true);
  const [atividadesLike, setAtividadesLike] = useState([]);
  const change = navigation.getParam('Change');
  const [points, setPoints] = useState();
  const nivelFinalizado = navigation.getParam('nivelFinalizado');
  const ativiades = navigation.getParam('atividades');
  const id = navigation.state.params.id;
  const dispatch = useDispatch();

  useEffect(() => {
    setLoading(true);

    async function loadLevel() {
      try {
        const res = await api.get('/v1/pointsActivity');
        setPoints(res.data);
        const response = await api.get('v1/atividades');
        setLevel(response.data.atividades);
        
      } catch (error) {
        if (error.response.data.message === 'Token has expired') {
          dispatch(signOut());
        }
      }
      setLoading(false);
    }

    loadLevel();

    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 20,
      }),

      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
      }),
    ]).start();
  }, [ativiades, change]);

  

  async function nivel() {
    const response = await api.get('v1/niveis');
    setModal(false);
    navigation.navigate('Level', {nivel: response.data.niveis});
  }

  //likes
  async function likes(id) {
    await api.post(`v1/like?atividade_id=${id}`);
    const response = await api.get('v1/atividades');
    setLevel(response.data.atividades);
  }

  return (
    <SafeAreaView>
      <Header navigation={navigation} points={points} />

      {nivelFinalizado === 1 && (
        <Modal isVisible={modal} animationIn="fadeInLeft">
          <View style={styles.modal}>
            <Lottie
              source={Winner}
              autoPlay
              loop={false}
              resizeMode="contain"
              style={{height: 200}}
            />
            <Text style={{color: 'white', fontWeight: 'bold'}}>
              Parabéns você completou um nivel
            </Text>
            <TouchableOpacity
              onPress={() => {
                nivel();
              }}
              style={styles.close}>
              <Text style={styles.textButton}>OK</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      )}

      <View
        style={{
          backgroundColor: '#f5f6f7',
          height: 40,
          flexDirection: 'row',
          alignItems: 'center',
          padding: 10,
        }}>
        <TouchableOpacity
          style={{flexDirection: 'row', alignItems: 'center'}}
          onPress={() => {
            nivel();
          }}>
          <Back
            style={{alignItems: 'center'}}
            color="#365899"
            name="ios-arrow-back"
            size={15}
          />
          <Text style={{color: '#365899', fontSize: 15, marginLeft: 10}}>
            Níveis
          </Text>
        </TouchableOpacity>
      </View>

      {loading === true ? (
        <View
          style={{padding: 30, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator color="#3078be" size={30} />
        </View>
      ) : (
        <FlatList
          data={level}
          renderItem={({item}) => (
            <Posts
              items={item}
              navigation={navigation}
              id={id}
              clickLike={id => likes(id)}
            />
          )}
          keyExtractor={item => item.id}
          contentContainerStyle={{paddingBottom: 120}}
        />
      )}
    </SafeAreaView>
  );
}

Timeline.navigationOptions = {
  header: null,
};
