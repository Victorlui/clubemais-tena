import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    marginTop: 8,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
    marginTop: 20,
  },
  title: {
    marginLeft: 2,
  },
  time: {
    fontSize: 10,
  },
  description: {
    fontSize: 12,
    width: 330,
  },
  icon: {
    marginRight: 15,
  },
  points: {
    backgroundColor: '#20315f',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    padding: 10,
  },

  boxLike: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  modal: {
    backgroundColor: 'white',
    height: 100,
    width: '100%',
    borderRadius: 8,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
  },
  warning: {
    textAlign: 'center',
    marginTop: 15,
    fontSize: 16,
  },
  close: {

    borderTopColor: '#d3d3d3',
    alignItems: 'center',
    width: '100%',
    marginTop:10
  },
  textButton:{
    fontSize:18,
    color:"#FBCC00"
  },
  modal: {
     
    height: 320,
    width: '100%',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    
  },
});

export default styles;
