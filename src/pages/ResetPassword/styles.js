import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems: 'center',
  },

  content: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
  },

  form: {
    display: 'flex',
    alignItems: 'center',
  },

  title: {
    fontSize: 32,
    color: '#fff',
    textAlign: 'center',
  },

  subTitle: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
    marginVertical:10
  },

  logo: {
    width: 280,
    height: 150,
    

  },

  input: {
    backgroundColor: '#fff',
    borderRadius: 20,
    width: 250,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 6},
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 10,
  },
  button: {
    width: 200,
    backgroundColor: '#0b2f94',
    marginTop: 30,
    borderRadius: 20,
    alignItems: 'center',
    padding: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },

  textButton: {
    color: '#ffff',
    fontWeight: 'bold',
  },
  terms: {
    color: '#1349ca',
    marginTop:40
  },
});


export default styles