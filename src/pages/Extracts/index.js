import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';

//api
import api from '../../services/api';

//libs
import {
  View,
  Text,
  Animated,
  ScrollView,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';

//estilos
import styles from './styles';

//Components
import Header from '../../components/header';


export default function Extracts({navigation}) {
  const [offset, setOffset] = useState(new Animated.ValueXY({x: 0, y: 50}));
  const [opacity, setOpacity] = useState(new Animated.Value(0));
  const [extratos, setExtratos] = useState([]);
  const [category, setCategory] = useState([]);
  const auth = useSelector(state => state.auth);
  const extract = navigation.getParam('extract');
  const saldo = navigation.getParam('saldo');
  const [points, setPoints] = useState();

  useEffect(() => {
    async function loadExtratos() {
      const response = await api.get('v1/extrato');
      setExtratos(response.data);

      const resp = await api.get('/v1/pointsActivity');
      setPoints(resp.data);

      const res = await api.get('v1/rewards');
      setCategory(res.data.rewards);
    }

    loadExtratos();
    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 20,
      }),

      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
      }),
    ]).start();
  }, [extract]);

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        <View style={styles.container}>
          <Header navigation={navigation} points={points} />
          <View style={styles.header}>
            <Text style={styles.rescue}>Extrato</Text>
            <View style={styles.points}>
              <Text style={styles.available}>Saldo Disponivel:</Text>
              <Text style={styles.pointsNumber}>{saldo}</Text>
            </View>
          </View>

          <View style={styles.reward}>
            {extratos.length === 0 ? (
              <ActivityIndicator color="blue" size={20} />
            ) : extratos.extrato.length === 0 ? (
              <View style={{alignItems: 'center', marginTop: 30}}>
                <Text style={{fontSize: 18, color: '#22222'}}>
                  Nenhum extrato encontrado.
                </Text>
              </View>
            ) : (
              extratos.extrato.map(items => (
                <>
                  {items.type === 'credito' ? (
                    <Animated.View
                      key={items.id}
                      style={[
                        {transform: [{translateY: offset.y}]},
                        {opacity: opacity},
                      ]}>
                      <View style={styles.credito}>
                        <View>
                          <View style={styles.nivel}>
                            <Text style={styles.textDebito}>Crédito</Text>
                            <Text style={styles.textBeginner}>
                              {items.origin}
                            </Text>
                          </View>
                          <Text style={styles.textDebito}>{items.date}</Text>
                          <View style={{top: 10, marginBottom: 10}}>
                            <Text>Expira em:</Text>
                            <Text style={styles.textDebito}>
                              {items.expire}
                            </Text>
                          </View>
                        </View>
                        <View>
                          <Text style={{fontSize: 20}}>Valor</Text>
                          <Text style={styles.textDebito}>{items.valor}</Text>
                        </View>
                      </View>
                    </Animated.View>
                  ) : (
                    <Animated.View
                      key={items.id}
                      style={[
                        {transform: [{translateY: offset.y}]},
                        {opacity: opacity},
                      ]}>
                      <View style={styles.debito}>
                        <View style={styles.date}>
                          <View>
                            <Text style={styles.textDebito}>Débito</Text>
                            <Text style={styles.textDebito}>{items.date}</Text>
                          </View>

                          <View>
                            <Text style={{fontSize: 20}}>Valor</Text>
                            <Text style={styles.textDebito}>{items.valor}</Text>
                          </View>
                        </View>

                        <View style={styles.lineStyle} />

                        <View style={styles.date}>
                          <View>
                            <Text style={{fontSize: 18}}>{items.reward}</Text>
                          </View>

                          <View>
                            <View style={styles.status}>
                              <Text style={styles.textStatus}>Status:</Text>
                              {items.status === 0 && (
                                <Text style={styles.textProsses}>
                                  Solicitado
                                </Text>
                              )}

                              {items.status === 1 && (
                                <Text style={styles.textProsses}>
                                  Processando
                                </Text>
                              )}

                              {items.status === 2 && (
                                <Text style={styles.textProsses}>
                                  Debitado
                                </Text>
                              )}
                            </View>
                          </View>
                        </View>
                      </View>
                    </Animated.View>
                  )}
                </>
              ))
            )}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

Extracts.navigationOptions = {
  header: null,
};
