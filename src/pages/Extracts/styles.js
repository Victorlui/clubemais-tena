import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    header: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      padding: 10,
      marginTop: 40,
    },
    points: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    pointsNumber: {
      fontWeight: 'bold',
      marginLeft:5,
      fontSize: 16,
    },
    rescue: {
      fontSize: 20,
      color: '#001977',
      fontWeight: 'bold',
    },
    available: {
      color: '#5a5a5a',
    },
    reward: {
      flex: 1,
    },
    picker: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: '#fff',
      marginTop: 10,
     
      height: 40,
      borderRadius: 10,
      padding: 10,
      borderWidth: 1,
      borderColor: '#d3d3d3',
      width: '80%',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 5,
    },
    debito: {
      backgroundColor: '#ff8680',
      borderRadius: 10,
      padding: 10,
      margin: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 5,
    },
    date: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginTop: 10,
      marginLeft: 10,
      marginRight: 10,
    },
    status: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    textDebito: {
      fontWeight: 'bold',
      fontSize: 20,
    },
    textStatus: {
      fontWeight: 'bold',
      fontSize: 12,
      textAlign: 'center',
    },
    textProsses: {
      fontSize: 12,
      textAlign: 'center',
      marginLeft: 3,
    },
    credito: {
      backgroundColor: '#9bd46c',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      borderRadius: 10,
      padding: 10,
      paddingLeft: 10,
      paddingRight: 10,
      margin: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 6},
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 5,
    },
    nivel: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    textBeginner: {
      marginLeft: 10,
    },
    lineStyle: {
      borderWidth: 0.5,
      borderColor: 'white',
      margin: 10,
    },
  });

  export default styles