import React, {useState, useEffect} from 'react';

import {
  View,
  Text,
  Image,
  TouchableOpacity,
  SafeAreaView,
  Animated,
  ActivityIndicator,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import logo from '../../assets/logo.png';
import LinearGradient from 'react-native-linear-gradient';
import {TextInputMask} from 'react-native-masked-text';
import {WSnackBar, WToast} from 'react-native-smart-tip';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import styles from './styles';
import api from '../../services/api';

export default function FirstAcess({navigation}) {
  const [cpfPoint, setCpfPoint] = useState('');
  const [loading, setLoading] = useState(false);
  const [offset, setOffset] = useState(new Animated.ValueXY({x: 0, y: 50}));
  const [opacity, setOpacity] = useState(new Animated.Value(0));
  const [error, setError] = useState('');

  useEffect(() => {
    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 20,
      }),

      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
      }),
    ]).start();
  }, []);

  async function onSubmit(cpf) {
    setLoading(true);

    if (cpf === '') {
      const snackBarOpts = {
        data: 'Preencha o cpf.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
      return null;
    }

    try {
      const cpf1 = cpf.replace(/[^\d]+/g, '');
      const response = await api.post(`v1/checkCpf?cpf=${cpf1}`);

      navigation.navigate('CreatePassword', {response: response});
      setLoading(false);
    } catch (error) {
      setLoading(false);
      const snackBarOpts = {
        data: 'CPF já utilizado ou inválido',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
    }

    setCpfPoint('');
  }

  return (
    <LinearGradient
    start={{x: 0.5, y: 0.1}}
    end={{x: 0.5, y: 0.8}}
    colors={['#1247c2', '#7dbe4c']}
    locations={[0.5, 0.5]}
      style={styles.container}>

      <SafeAreaView>
        <KeyboardAvoidingView  behavior="padding" enabled>
        <View style={styles.content}>
          <View>
            <Image style={styles.logo} resizeMode="contain" source={logo} />

            <Text style={{color: '#fff', textAlign: 'center'}}>
              VANTAGENS E BENEFÍCIOS EXCLUSIVOS
            </Text>

            <View style={([styles.form], {marginVertical: 20})}>
              <Text style={styles.title}>Bem vindo (a)!</Text>
            </View>
          </View>

          <View style={styles.form}>
            <Text style={styles.subTitle}>
              Digite seu CPF e clique em verificar
            </Text>
            <TextInputMask
              type={'cpf'}
              style={styles.input}
              value={cpfPoint}
              onChangeText={text => {
                setCpfPoint(text);
              }}
              placeholder="CPF"
            />

            {loading ? (
              <ActivityIndicator color="white" />
            ) : (
              <>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => {
                    onSubmit(cpfPoint);
                  }}>
                  <Text style={styles.textButton}>VERIFICAR</Text>
                </TouchableOpacity>
              </>
            )}
          </View>

          {error === '' ? null : <Text>{error}</Text>}

          <View style={{alignItems: 'center', justifyContent: 'space-between'}}>
            <TouchableOpacity
              style={{marginTop: 10}}
              onPress={() => {
                navigation.goBack();
              }}>
              <Text style={{color: '#1349ca'}}>Já possui cadastro? Entrar</Text>
            </TouchableOpacity>
            <View>
              <Text style={styles.terms}>
                Todos os direitos reservados a Essity
              </Text>
            </View>
          </View>
        </View>
        </KeyboardAvoidingView>
       
      </SafeAreaView>
    </LinearGradient>
  );
}

FirstAcess.navigationOptions = {
  header: null,
};
