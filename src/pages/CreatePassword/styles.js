import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#7dbe4c',
  },

  header: {
    backgroundColor: '#1040b0',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
  },

  body: {
    backgroundColor: '#7dbe4c',
    height: 'auto',
    display: 'flex',
    alignItems: 'center',
  },

  title: {
    marginTop: -30,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },

  subTitle: {
    fontSize: 16,
    color: '#fff',
    marginBottom: 20,
  },

  logo: {
    width: 250,
    height: 190,
    resizeMode:'contain',
    marginBottom:10
   
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  input: {
    backgroundColor: '#fff',
    borderRadius: 20,
    width: '80%',
    marginTop: 20,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 6},
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 10,
  },
  button: {
    width: 200,
    backgroundColor: '#0b2f94',
    marginTop: 30,
    borderRadius: 20,
    alignItems: 'center',
    padding: 10,
    borderWidth: 1,
    borderColor: '#fff',
    marginBottom: 20,
  },

  textButton: {
    color: '#ffff',
    fontWeight: 'bold',
  },
  terms: {
    color: '#1349ca',
    marginTop: 40,
  },
});

export default styles;
