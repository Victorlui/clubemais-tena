import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {signInRequest} from '../../store/modules/auth/actions';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  SafeAreaView,
  Animated,
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';

import {TextInputMask} from 'react-native-masked-text';
import {WSnackBar, WToast} from 'react-native-smart-tip';

import logo from '../../assets/logo.png';

import styles from './styles';
import api from '../../services/api';

import OneSignal from 'react-native-onesignal';

export default function CreatePassword({navigation}) {
  const nameRef = useRef();
  const emailRef = useRef();
  const telephoneRef = useRef();
  const telRef = useRef();
  const passwordRef = useRef();
  const confirmRef = useRef();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [cell, setCell] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPass, setConfirmPass] = useState('');
  const [loading, setLoading] = useState(false);
  const [offset, setOffset] = useState(new Animated.ValueXY({x: 0, y: 50}));
  const [opacity, setOpacity] = useState(new Animated.Value(0));
  const user = navigation.state.params.response;
  const dispatch = useDispatch();

  if (!user) return null;

  useEffect(() => {
    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 5,
        bounciness: 20,
      }),

      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
      }),
    ]).start();
  }, []);

  async function onSubmit(name, email, cell, password, confirmPass) {
    const {cpf, store_id, type} = user.data.user;
    const cell1 = cell.replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, '');

    setLoading(true);
    if (
      name === '' &&
      cell === '' &&
      password === '' &&
      confirmPass === '' &&
      email === ''
    ) {
      const snackBarOpts = {
        data: 'Preencha os campos.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
      return null;
    }

    if (email === '') {
      const snackBarOpts = {
        data: 'Preencha o E-mail.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
      return null;
    }

    if (name === '') {
      const snackBarOpts = {
        data: 'Preencha o Nome.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);

      setLoading(false);
      return null;
    }

    if (cell === '') {
      const snackBarOpts = {
        data: 'Preencha o Telefone.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
      return null;
    }

    if (password === '') {
      const snackBarOpts = {
        data: 'Preencha a senha.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
      return null;
    }

    if (confirmPass === '') {
      const snackBarOpts = {
        data: 'Confirme sua senha.',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
      return null;
    }

    if (password !== confirmPass) {
      const snackBarOpts = {
        data: 'Senhas não conferem',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
      return null;
    }

    try {
      const response = await api.post(
        'https://tena.ciadetrade.eco.br/api/v1/register',
        {name, email, password, type, store_id, cpf, celular: cell1},
      );

      let externalUserId = `${response.data.user.id}`; // You will supply the external user id to the OneSignal SDK

      // Setting External User Id with Callback Available in SDK Version 3.7.0+
      OneSignal.setExternalUserId(externalUserId);

      if (response.status === 200) {
        dispatch(signInRequest(cpf, password));
        setLoading(false);
      }

      setLoading(false);
    } catch (error) {
      const snackBarOpts = {
        data: 'Email já utilizado',
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: 'red',
      };
      WToast.show(snackBarOpts);
      setLoading(false);
    }
  }

  return (
    <SafeAreaView style={{flex:1}}>
       <ScrollView style={styles.container}>
      <StatusBar backgroundColor="#1040b0" barStyle="light-content" />
      <KeyboardAvoidingView
        enabled={Platform.OS === 'ios' && true}
        behavior="padding">
        <Animated.View
          style={[{transform: [{translateY: offset.y}]}, {opacity: opacity}]}>
          <View style={styles.header}>
            <Image style={styles.logo} source={logo} />

            <View style={styles.title}>
              <Text style={{fontSize: 24, color: '#fff'}}>Olá !</Text>
              <Text style={{fontSize: 14, color: '#fff'}}>
                Preencha os dados abaixo para realizar
              </Text>
              <Text style={{fontSize: 14, color: '#fff'}}>
                seu primeiro acesso
              </Text>
            </View>
          </View>

          <View style={styles.body}>
            <View style={{margin: 10, width: '80%'}}>
              <Text style={{color: '#fff'}}>Digite o seu nome:</Text>
              <TextInput
                style={{
                  marginTop: 10,
                  height: 40,
                  backgroundColor: '#fff',
                  borderRadius: 15,
                }}
                textAlign={'center'}
                placeholder="Nome Completo"
                value={name}
                onChangeText={text => setName(text)}
                returnKeyType="next"
                ref={nameRef}
                onSubmitEditing={() => {
                  emailRef.current.focus();
                }}
                autoCorrect={false}
              />
            </View>

            <View style={{margin: 10, width: '80%'}}>
              <Text style={{color: '#fff'}}>Digite seu E-mail:</Text>
              <TextInput
                style={{
                  marginTop: 10,
                  height: 40,
                  backgroundColor: '#fff',
                  borderRadius: 15,
                }}
                keyboardType='email-address'
                textAlign={'center'}
                placeholder="E-mail"
                value={email}
                autoCapitalize="none"
                onChangeText={text => setEmail(text)}
                returnKeyType="next"
                ref={emailRef}
              />
            </View>

            <View style={{margin: 10, width: '80%'}}>
              <Text style={{color: '#fff'}}>
                Digite o seu número de celular:
              </Text>
              <TextInputMask
                type={'cel-phone'}
                options={{
                  maskType: 'BRL',
                  withDDD: true,
                  dddMask: '(99) ',
                }}
                style={{
                  marginTop: 10,
                  height: 40,
                  backgroundColor: '#fff',
                  borderRadius: 15,
                }}
                textAlign={'center'}
                value={cell}
                onChangeText={text => {
                  setCell(text);
                }}
                placeholder="Celular"
                returnKeyType="next"
                ref={ref => telephoneRef }
                onSubmitEditing={() => {
                  passwordRef.current.focus();
                }}
              />
            </View>

            <View style={{margin: 10, width: '80%'}}>
              <Text style={{color: '#fff'}}>
                Crie uma senha para seu acesso:
              </Text>
              <TextInput
                style={{
                  marginTop: 10,
                  height: 40,
                  backgroundColor: '#fff',
                  borderRadius: 15,
                }}
                textAlign={'center'}
                placeholder="Senha"
                value={password}
                onChangeText={text => setPassword(text)}
                secureTextEntry
                returnKeyType="next"
                ref={passwordRef}
                onSubmitEditing={() => {
                  confirmRef.current.focus();
                }}
              />
            </View>

            <View style={{margin: 10, width: '80%'}}>
              <Text style={{color: '#fff'}}>Repita a senha que criou:</Text>
              <TextInput
                style={{
                  marginTop: 10,
                  height: 40,
                  backgroundColor: '#fff',
                  borderRadius: 15,
                }}
                textAlign={'center'}
                placeholder="Confirme sua Senha"
                secureTextEntry
                value={confirmPass}
                onChangeText={text => setConfirmPass(text)}
                returnKeyType="send"
                ref={confirmRef}
                onSubmitEditing={() => {
                  onSubmit(name, email, cell, password, confirmPass);
                }}
              />
            </View>

            {loading ? (
              <ActivityIndicator color="blue" style={{marginTop:5}}  />
            ) : (
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  onSubmit(name, email, cell, password, confirmPass);
                }}>
                <Text style={styles.textButton}>ENTRAR</Text>
              </TouchableOpacity>
            )}
          </View>
        </Animated.View>
      </KeyboardAvoidingView>
    </ScrollView>
    </SafeAreaView>
   
  );
}

CreatePassword.navigationOptions = {
  header: null,
};
