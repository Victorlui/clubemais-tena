import produce from 'immer';

const INITIAL_STATE = {
  token: null,
  signed: false,
  loading: false, 
  id:null,
  error:null,
  dados:null
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case '@auth/SIGN_IN_REQUEST':
      return produce(state, draft => {
        (draft.error = null), (draft.loading = true)
      }); 
    case '@auth/SIGN_IN_SUCCESS':
      return produce(state, draft => {
        (draft.token = action.payload.token), (draft.signed = true), (draft.dados = action.payload.configs), (draft.loading = false)
      }); 
      case '@auth/SIGN_FAILURE':
      return produce(state, draft => {
        (draft.loading = false)
      }); 
    case '@auth/SIGN_OUT':
      return produce(state, draft => {
        (draft.token = null), (draft.signed = false);
      });
      case 'verifica':
      return produce(state, draft => {
        (draft.id = action.payload.id)
      });

    default:
      return state;
  }
}
