export function signInRequest(cpfPoint, password) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: {cpfPoint, password},
  };
}

export function signInSuccess(token, user, configs) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: {token, user, configs},
  };
}

export function signFailure() {
  return {
    type: '@auth/SIGN_FAILURE'
  };
}
 
export function signOut() {
  return {
    type: '@auth/SIGN_OUT',
  };

}

export function verifica(id) {
  return {
    type: 'verifica',
    payload: {id}
  };
}
