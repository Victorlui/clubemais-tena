import {takeLatest, call, all, put} from 'redux-saga/effects';

import {signInSuccess,signFailure} from './actions';

import {WSnackBar, WToast} from 'react-native-smart-tip';

import api from '../../../services/api';
import OneSignal from 'react-native-onesignal';

export function* signIn({payload}) {
  const {cpfPoint, password} = payload;

   const cpf = cpfPoint.replace(/[^\d]+/g, '');

 try{
    const response = yield call(api.post, 'v1/auth', {
        cpf,
        password,
      });

      const {token,user} = response.data
      let externalUserId = `${user.id}`; // You will supply the external user id to the OneSignal SDK
      // Setting External User Id with Callback Available in SDK Version 3.7.0+
      OneSignal.setExternalUserId(externalUserId); 
      const configs = response.data
      api.defaults.headers.Authorization = `Bearer ${token}`
      
      yield put(signInSuccess(token,user,configs))

 }catch(error){
  const snackBarOpts = {
    data: 'Credenciais Inválida',
    position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
    duration: WSnackBar.duration.SHORT, //1.SHORT 2.LONG 3.INDEFINITE
    textColor: '#fff',
    backgroundColor: 'red',
  };
  WToast.show(snackBarOpts);
  yield put(signFailure())

 }
 
}

export function setToken({payload}){
  if(!payload) return

  const {token} = payload.auth

  

  if(token){
    api.defaults.headers.Authorization = `Bearer ${token}`
  }
}


export default all([
  takeLatest('persist/REHYDRATE',setToken),
  takeLatest('@auth/SIGN_IN_REQUEST', signIn)
]);
