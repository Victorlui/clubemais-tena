import {takeLatest, call, put, all} from 'redux-saga/effects'

import {UpdadeSuccess,UpdateFailure} from './actions';
import {NavigationActions} from 'react-navigation'

import {WSnackBar, WToast} from 'react-native-smart-tip';

import api from '../../../services/api';

export function* update({payload}){

  try{
    const {name,email,cell,type} = payload;
    const response = yield call(api.post,'v1/editUser',{
        name:name,
        email:email,
        celular: cell
      })

      const user = {
          name: response.data.user.name,
          email: response.data.user.email,
          celular: response.data.user.celular,
          type: type
      }
     

      yield put(UpdadeSuccess(user))

      const snackBarOpts = {
        data: "Perfil atualizado",
        position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
        duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
        textColor: '#fff',
        backgroundColor: '#04911B',
      };
      WToast.show(snackBarOpts);

  }catch(error){
    const snackBarOpts = {
      data: 'Verifique seus dados.',
      position: WSnackBar.position.TOP, // 1.TOP 2.CENTER 3.BOTTOM
      duration: WSnackBar.duration.LONG, //1.SHORT 2.LONG 3.INDEFINITE
      textColor: '#fff',
      backgroundColor: 'red',
    };
    WToast.show(snackBarOpts);
   
  }
}

export default  all([
    takeLatest("@user/UPDATE_REQUEST",update)
])