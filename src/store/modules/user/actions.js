export function UpdateRequest(name,email,cell,type){
    return{
        type: '@user/UPDATE_REQUEST',
        payload: {name,email,cell,type}
    }
}

export function UpdadeSuccess(user){
    return{
        type: '@user/UPDATE_SUCCESS',
        payload: {user}
    }
}

export function UpdateFailure(status){
    return{
        type: '@user/UPDATE_FAILURE',
        payload:{status}
    }
}