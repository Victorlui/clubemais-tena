import {all} from 'redux-saga/effects'

import auth from './auth/sagas'
import user from './user/sagas'
import checkCpf from './checkCpf/sagas'

export default function* rooteSaga(){
    return yield all([auth,user,checkCpf])
}