import {takeLatest, call, all, put} from 'redux-saga/effects';

import {CheckCpfSuccess,CheckCpfFailure} from './actions';
import {NavigationActions} from 'react-navigation'

import api from '../../../services/api';

export function* checkCpf({payload}){
  const {cpfPoint} = payload;

  const cpf = cpfPoint.replace(/[^\d]+/g, '');

  try{
    const response = yield call(api.post, `v1/checkCpf?cpf=${cpf}`);

    
    yield put(CheckCpfSuccess(response))
    
    
  }catch(error){
    const {status} = error.response.data
  
    yield put(CheckCpfFailure(status))
   
  }
}

export default all([
  takeLatest('@auth/CHECK_CPF_REQUEST', checkCpf)
]);
