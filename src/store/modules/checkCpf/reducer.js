import produce from 'immer'

const INITIAL_STATE = {
    status:null,
    data:null
}

export default function checkCpf (state = INITIAL_STATE , action){
    switch(action.type){
        case '@auth/CHECK_CPF_SUCCESS':
                return produce(user, draft => {
                    draft.data = action.payload.data
                })
        case '@auth/CHECK_CPF_FAILURE':
                return produce(state, draft => {
                     draft.status = action.payload.status
                })
        default:
            return state
    }
}