export function CheckCpfRequest(cpfPoint){
    return{
        type: '@auth/CHECK_CPF_REQUEST',
        payload: {cpfPoint}
    }
}

export function CheckCpfSuccess(data){
    return{
        type: '@auth/CHECK_CPF_SUCCESS',
        payload: {data}
    }
}

export function CheckCpfFailure(status){
    return{
        type: '@auth/CHECK_CPF_FAILURE',
        payload:{status}
    }
}